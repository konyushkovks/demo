{
  "parser": "babel-eslint",
  "plugins": [
    "html"
  ],
  "env": {
    "browser": true,
    "node": true,
    "mocha": true
  },
  "globals": {"Promise": true, "expect": true},

  "extends": "eslint:recommended",
  "rules": {
    "no-await-in-loop": "error",
    "no-extra-parens": "error",
    "no-prototype-builtins": "error",
    "no-template-curly-in-string": "error",
    "accessor-pairs": "error",
    "block-scoped-var": "error",
    "class-methods-use-this": "error",
    "consistent-return": "error",
    "curly": "error",
    "default-case": "error",
    "dot-location": ["error", "object"],
    "dot-notation": "error",
    "eqeqeq": "error",
    "guard-for-in": "error",
    "no-alert": "error",
    "no-caller": "error",
    "no-empty-function": ["error", { "allow": ["arrowFunctions"] }],
    "no-else-return": [ "error", { "allowElseIf": true }],
    "no-eq-null": "error",
    "no-eval": "error",
    "no-extra-label": "error",
    "no-floating-decimal": "error",
    "no-implicit-coercion": "error",
    "no-implicit-globals": "error",
    "no-implied-eval": "error",
    "no-iterator": "error",
    "no-labels": "error",
    "no-lone-blocks": "error",
    "no-loop-func": "error",
    "no-multi-spaces": "error",
    "no-multi-str": "error",
    "no-new": "error",
    "no-new-func": "error",
    "no-new-wrappers":"error",
    "no-octal-escape": "error",
    "no-param-reassign": "error",
    "no-proto": "error",
    "no-return-await": "error",
    "no-script-url": "error",
    "no-self-compare": "error",
    "no-throw-literal": "error",
    "no-unused-expressions": "error",
    "no-useless-concat": "error",
    "no-useless-return": "error",
    "no-void": "error",
    "no-with": "error",
    "prefer-promise-reject-errors": "error",
    "radix": "error",
    "vars-on-top": "error",
    "yoda": "error",
    "array-bracket-spacing": ["error", "never"],
    "array-element-newline": ["error", "consistent"],
    "block-spacing": "error",
    "brace-style": ["error", "stroustrup"],
    "comma-dangle": ["error", "never"],
    "comma-spacing": "error",
    "comma-style": "error",
    "computed-property-spacing": "error",
    "consistent-this": ["error", "self"],
    "eol-last": ["error", "always"],
    "func-call-spacing": ["error", "never"],
    "func-style": ["error", "declaration", { "allowArrowFunctions": true }],
    "function-paren-newline": ["error", "never"],
    "indent": ["error", 4, { "SwitchCase": 1 }],
    "key-spacing": "error",
    "keyword-spacing": "error",
    "line-comment-position": "error",
    "lines-between-class-members": "error",
    "max-depth": ["error", 5],
    "max-len": ["error", {"code": 140}],
    "max-params": ["error", 3],
    "max-statements-per-line": "error",
    "new-parens": "error",
    "no-bitwise": "error",
    "no-continue": "error",
    "no-lonely-if": "error",
    "no-mixed-operators": "error",
    "no-multi-assign": "error",
    "no-multiple-empty-lines": ["error", {"max": 1, "maxEOF": 0, "maxBOF": 0 }],
    "no-nested-ternary": "error",
    "no-new-object": "error",
    "no-plusplus": ["error", { "allowForLoopAfterthoughts": true }],
    "no-tabs": "error",
    "no-trailing-spaces": "error",
    "no-unneeded-ternary": "error",
    "no-whitespace-before-property": "error",
    "object-curly-newline" : ["error", { "consistent": true }],
    "object-curly-spacing": ["error"],
    "one-var": ["error", { "var": "never", "let": "never", "const": "never" }],
    "one-var-declaration-per-line": "error",
    "operator-assignment": "error",
    "operator-linebreak": "error",
    "padded-blocks": ["error", "never"],
    "padding-line-between-statements": [
      "error",
      { "blankLine": "never", "prev": "*", "next": "*" },
      { "blankLine": "always", "prev": "*", "next": "return" },
      { "blankLine": "always", "prev": "import", "next": "*"},
      { "blankLine": "always", "prev": "const", "next": "export"},
      { "blankLine": "always", "prev": "const", "next": "expression"},
      { "blankLine": "always", "prev": "expression", "next": "export"},
      { "blankLine": "always", "prev": "expression", "next": "const"},
      { "blankLine": "always", "prev": "const", "next": "block-like"},
      { "blankLine": "always", "prev": "export", "next": "*"},
      { "blankLine": "always", "prev": "function", "next": "*"},
      { "blankLine": "any", "prev": "import", "next": "import"}
    ],
    "quote-props": ["error", "as-needed"],
    "quotes": "error",
    "semi": "error",
    "semi-spacing": "error",
    "semi-style": "error",
    "space-before-blocks": "error",
    "space-before-function-paren": ["error", "never"],
    "space-infix-ops": ["error", {"int32Hint": false}],
    "space-unary-ops": ["error", {"words": true, "nonwords": false}],
    "spaced-comment": "error",
    "switch-colon-spacing": "error",
    "unicode-bom": "error",
    "wrap-regex": "error",
    "arrow-body-style": ["error", "as-needed"],
    "arrow-parens": "error",
    "arrow-spacing": "error",
    "no-confusing-arrow": "error",
    "no-duplicate-imports": "error",
    "no-useless-computed-key": "error",
    "no-var": "error",
    "object-shorthand": "error",
    "prefer-arrow-callback": "error",
    "prefer-const": "error",
    "prefer-rest-params": "error",
    "prefer-spread": "error",
    "prefer-template": "error",
    "rest-spread-spacing": "error",
    "template-curly-spacing": "error"
  }
}
