"use strict";
const webpack = require("webpack");
const baseConfig = require("./webpack.base.config");
const merge = require("webpack-merge");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const config = merge(baseConfig, {
    mode: "development",

    devtool: "#cheap-module-source-map",

    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /node_modules/,
                    chunks: "initial",
                    name: "vendors"
                },
                styles: {
                    name: "css/main",
                    test: (module) =>
                        module.nameForCondition &&
                        (/\.(s?css|vue)$/).test(module.nameForCondition()) && !(/^javascript/).test(module.type),
                    chunks: "all",
                    enforce: true
                }
            }
        }
    },

    devServer: {
        hot: true,
        watchOptions: {
            poll: true
        },
        historyApiFallback: {
            rewrites: [
                { from: /^\/$/, to: "/" },
                { from: /^\/profile/, to: "/" }
            ]
        }
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: "./src/index.html",
            inject: true
        })
    ]
});

module.exports = config;
