"use strict";
const baseConfig = require("./webpack.base.config");
const merge = require("webpack-merge");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const config = merge(baseConfig, {
    mode: "production",

    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /node_modules/,
                    chunks: "initial",
                    name: "vendors"
                },
                styles: {
                    name: "css/styles",
                    test: (module) =>
                        module.nameForCondition &&
                        (/\.(s?css|vue)$/).test(module.nameForCondition()) && !(/^javascript/).test(module.type),
                    chunks: "all",
                    enforce: true
                }
            }
        },
        minimizer: [
            new UglifyJsPlugin({
                cache: true,
                parallel: true
            }),
            new OptimizeCSSAssetsPlugin({})
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: "src/index.html"
        })
    ]

});

module.exports = config;
