import {http} from "~http";

export async function findProfile() {
    let entity;
    await http({
        method: "get",
        url: "/api/profile"
    }).then((response) => {
        entity = response.data;
    }).catch(() => {
        entity = null;
    });

    return entity;
}

export async function update(profile) {
    return http({
        method: "post",
        url: "/api/profile/personal/update",
        data: profile
    });
}
