import {Types} from "./const";

export default {
    [Types.mutations.SET_LOGGED_IN]: (state, isLoggedIn) => {
        state.isLoggedIn = isLoggedIn;
    },

    [Types.mutations.SET_READY]: (state, isReady) => {
        state.isReady = isReady;
    },

    SET_TEST: (state, test) => {
        state.test = test;
    }
};
