export const namespace = "app";

export const Types = {
    getters: {
        isLoggedIn: "isLoggedIn",
        isReady: "isReady"
    },

    mutations: {
        SET_LOGGED_IN: "SET_LOGGED_IN",
        SET_READY: "SET_READY"
    }
};
