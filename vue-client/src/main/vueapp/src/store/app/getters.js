import {Types} from "./const";

export default {
    [Types.getters.isLoggedIn](state) {
        return state.isLoggedIn;
    },

    [Types.getters.isReady](state) {
        return state.isReady;
    }
};
