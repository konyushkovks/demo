import Vue from "vue";
import Vuex from "vuex";
import {namespace as appNamespace} from "./app/const";
import appModule from "./app/module";
import {namespace as profileNamespace} from "./profile/const";
import profileModule from "./profile/module";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        [appNamespace]: appModule,
        [profileNamespace]: profileModule
    }
});
