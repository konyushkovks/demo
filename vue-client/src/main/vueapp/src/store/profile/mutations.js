import {Types} from "./const";

export default {
    [Types.mutations.SET]: (state, profile) => {
        state.profile = profile;
    }
};
