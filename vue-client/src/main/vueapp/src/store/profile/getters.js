import {Types} from "./const";

export default {
    [Types.getters.profile](state) {
        return state.profile;
    }
};
