import {Types} from "./const";
import {findProfile, update} from "@/api/UserService";

export default {
    async [Types.actions.FETCH]({commit}) {
        commit(Types.mutations.SET, await findProfile());
    },

    async [Types.actions.UPDATE]({commit}, profile) {
        update(profile).then((response) => {
            commit(Types.mutations.SET, response.data.payload);
        }).catch(() => {});
    }
};
