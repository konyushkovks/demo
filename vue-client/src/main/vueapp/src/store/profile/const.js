export const namespace = "profile";

export const Types = {
    getters: {
        profile: "profile"
    },

    mutations: {
        SET: "SET"
    },

    actions: {
        FETCH: "FETCH",
        UPDATE: "UPDATE"
    }
};
