import jwt_decode from "jwt-decode";
import axios from "axios";
import qs from "qs";
import {AUTH_SERVER_URL, VUE_CLIENT_URL} from "@/includes/properties";

const CLIENT_ID = "vue_client";
const SECRET = "appS3cr3T";
const SCOPE = "read";
const ACCESS_TOKEN = "access_token";
const REFRESH_TOKEN = "refresh_token";
const TOKEN_PAYLOAD = "token_payload";
const defaultHttp = axios.create({
    baseURL: AUTH_SERVER_URL
});

export function authenticateOnServer() {
    window.location.href =
        `${AUTH_SERVER_URL}/oauth/authorize` +
        "?response_type=code" +
        `&client_id=${CLIENT_ID}&redirect_uri=${VUE_CLIENT_URL}&scope=${SCOPE}`;
}

export function retrieveToken(code) {
    const data = {
        grant_type: "authorization_code",
        client_id: CLIENT_ID,
        redirect_uri: VUE_CLIENT_URL,
        code
    };

    defaultHttp({
        method: "post",
        url: `${AUTH_SERVER_URL}/oauth/token`,
        data: qs.stringify(data),
        headers: {
            "Content-type": "application/x-www-form-urlencoded; charset=utf-8",
            Authorization: `Basic ${_getClientEncoded()}`
        }
    }).then((response) => {
        _saveToken(response.data);
        _reloadWindow();
    }).catch(() => {});
}

export async function refreshToken() {
    const data = {
        grant_type: "refresh_token",
        refresh_token: _getRefreshToken(),
        scope: SCOPE
    };

    await defaultHttp({
        method: "post",
        url: `${AUTH_SERVER_URL}/oauth/token`,
        data: qs.stringify(data),
        headers: {
            "Content-type": "application/x-www-form-urlencoded",
            Authorization: `Basic ${_getClientEncoded()}`
        }
    }).then((response) => {
        _saveToken(response.data);
    }).catch(() => {});
}

export function checkCredentials() {
    return Boolean(localStorage.getItem(ACCESS_TOKEN));
}

export function getAccessToken() {
    return localStorage.getItem(ACCESS_TOKEN);
}

export function getTokenPayload() {
    try {
        const payload = localStorage.getItem(TOKEN_PAYLOAD);

        return payload ? payload : jwt_decode(getAccessToken());
    }
    catch (e) {
        return null;
    }
}

export function logout() {
    try {
        localStorage.removeItem(ACCESS_TOKEN);
        localStorage.removeItem(REFRESH_TOKEN);
        localStorage.removeItem(TOKEN_PAYLOAD);
        window.location.href = `${AUTH_SERVER_URL}/logout`;
    }
    catch (e) {
        _reloadWindow();
    }
}

function _saveToken(token) {
    try {
        localStorage.setItem(ACCESS_TOKEN, token.access_token);
        localStorage.setItem(REFRESH_TOKEN, token.refresh_token);
        localStorage.setItem(TOKEN_PAYLOAD, JSON.stringify(jwt_decode(token.access_token)));
    }
    catch (e) {
        // ignore
    }
}

function _getRefreshToken() {
    return localStorage.getItem(REFRESH_TOKEN);
}

function _getClientEncoded() {
    return btoa(`${CLIENT_ID}:${SECRET}`);
}

function _reloadWindow() {
    window.location.href = VUE_CLIENT_URL;
}
