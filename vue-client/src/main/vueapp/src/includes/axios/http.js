import axios from "axios";
import {getAccessToken, logout, refreshToken} from "@/services/AuthService";
import {eventHub} from "@/includes/eventHub";
import {addSuccessNotify, addWarnNotify} from "@lib/NotifyService";
import {AUTH_SERVER_URL} from "@/includes/properties";

export const http = axios.create({
    baseURL: AUTH_SERVER_URL
});

http.interceptors.request.use((config) => {
    const token = getAccessToken();

    if ( token !== null ) {
        config.headers.Authorization = `Bearer ${token}`;
    }

    return config;
}, (err) => Promise.reject(err));
http.interceptors.response.use((response) => {
    const method = response.config.method;

    if (method !== "get") {
        addSuccessNotify(response.data.notice);
    }

    return response;
}, async(error) => {
    const originalRequest = error.config;

    // 401
    if (error.response.status === 401 && !originalRequest._retry) {
        originalRequest._retry = true;
        await refreshToken();
        originalRequest.headers.Authorization = `Bearer ${getAccessToken()}`;

        return http(originalRequest);
    }
    else if (error.response.status === 401 && originalRequest._retry) {
        logout();
    }
    // 400
    else if (error.response.status === 400) {
        addWarnNotify(error.response.data.notice);
        _throwError(error.response.data.errors);
    }
    else {
        addWarnNotify(error.response.data.notice);
    }

    return Promise.reject(error);
});
function _throwError(errors) {
    eventHub.$emit("throwErrors", errors);
}
