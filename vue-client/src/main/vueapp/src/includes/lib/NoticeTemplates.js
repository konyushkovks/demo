export const constraintViolationsNotice = Object.freeze({
    title: "Произошла ошибка",
    message: "Проверьте правильность введенных данных"
});
