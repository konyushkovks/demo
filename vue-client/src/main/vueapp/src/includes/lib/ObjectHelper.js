export const pick = (O, ...K) => K.reduce((o, k) => (o[k] = O[k], o), {});

export const getCopy = (obj) => ({...obj});
