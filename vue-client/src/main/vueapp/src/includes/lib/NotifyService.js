import {eventHub} from "@/includes/eventHub";
import {Enum} from "enumify";
import {constraintViolationsNotice} from "@lib/NoticeTemplates";

export class NotifyStatus extends Enum {}

NotifyStatus.initEnum(["SUCCESS", "WARN", "ERROR"]);

export function addSuccessNotify(notice) {
    eventHub.$emit("addNotify", {
        status: NotifyStatus.SUCCESS,
        ...notice
    });
}

export function addWarnNotify(notice) {
    eventHub.$emit("addNotify", {
        status: NotifyStatus.WARN,
        ...notice
    });
}

export function addConstraintViolationsNotify() {
    addWarnNotify(constraintViolationsNotice);
}
