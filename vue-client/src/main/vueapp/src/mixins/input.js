export default {
    props: {
        title: {
            type: String,
            required: true
        },

        value: {
            required: true
        },

        required: {
            type: Boolean
        },

        validation: {
            type: Object
        },

        serverErrors: {
            type: Array
        }
    },

    data() {
        return {
            statesEnum: {
                DEFAULT: "default",
                ERROR: "error",
                VALID: "valid"
            }
        };
    },

    components: {
        LabelComponent: () => import("@/components/label.vue"),
        ElementComponent: () => import("@/components/element.vue"),
        ErrorComponent: () => import("@/components/error.vue")
    },

    methods: {
        update(event) {
            const value = event.target.value.trim();

            this.$emit("update:value", value.length > 0 ? value
                : null);
            if (this.validation) {
                this.validation.$touch();
            }
        }
    },

    computed: {
        state() {
            if (!this.validation || !this.validation.$dirty) {
                return this.statesEnum.DEFAULT;
            }
            else if (this.validation.$invalid) {
                return this.statesEnum.ERROR;
            }

            return this.statesEnum.VALID;
        },

        getErrors() {
            if (this.hasServerErrors) {
                return this.serverErrors.map((er) => er);
            }
            else if (this.hasValidationParams) {
                return Object.keys(this.validation.$params).reduce((result, key) => {
                    if (!this.validation[key]) {
                        result.push(this.validation.$params[key].msg);
                    }

                    return result;
                }, []);
            }

            return null;
        },

        hasServerErrors() {
            return Boolean(this.serverErrors) && this.serverErrors.length > 0;
        },

        hasValidationParams() {
            return Boolean(this.validation) && Boolean(this.validation.$params);
        }
    }
};
