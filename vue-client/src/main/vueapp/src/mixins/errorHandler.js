export default {
    data() {
        return {
            serverErrors: {}
        };
    },

    created() {
        this.$eventHub.$on("throwErrors", (errors) => this.handleErrors(errors));
    },

    methods: {
        handleErrors(errors) {
            this.serverErrors = errors;
        }
    }
};
