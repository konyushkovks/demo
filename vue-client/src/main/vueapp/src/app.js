import Vue from "vue";
import store from "@/store/store";
import {createRouter} from "@/router/router";
import App from "./App.vue";
import VueMq from "vue-mq";
import Vuelidate from "vuelidate";

Vue.use(Vuelidate);
Vue.use(VueMq, {
    breakpoints: {
        portraitPhone: 480,
        portraitNote: 767,
        landscapeNote: 980,
        standard: Infinity
    }
});

const router = createRouter();
const app = new Vue({ // eslint-disable-line no-unused-vars
    el: "#app",
    router,
    store,
    render: (h) => h(App)
});
