import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
    {path: "/", component: () => import("@/pages/profile/index.vue")}
];

export function createRouter() {
    return new VueRouter({
        mode: "history",
        routes
    });
}
