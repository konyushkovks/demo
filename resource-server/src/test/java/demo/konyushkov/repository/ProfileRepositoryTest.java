package demo.konyushkov.repository;

import com.vladmihalcea.sql.SQLStatementCountValidator;
import demo.konyushkov.config.IntegrationTestConfig;
import demo.konyushkov.model.Profile;
import demo.konyushkov.service.MessageService;
import demo.konyushkov.tools.TestConst;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Optional;

import static com.vladmihalcea.sql.SQLStatementCountValidator.assertSelectCount;
import static com.vladmihalcea.sql.SQLStatementCountValidator.assertUpdateCount;
import static org.junit.jupiter.api.Assertions.*;

class ProfileRepositoryTest {

    @Nested
    public class FindByUuid extends IntegrationTestConfig {
        @Autowired
        ProfileRepository profileRepository;

        @Test
        void When_ProfileExistInDb_Expect_ReturnProfile() {
            SQLStatementCountValidator.reset();
            Optional<Profile> profile = profileRepository.findByUuid(TestConst.UUID);
            assertSelectCount(1);

            assertTrue(profile.isPresent());
            assertEquals(TestConst.UUID, profile.get().getUuid());
        }
    }

    @Nested
    public class Update extends IntegrationTestConfig {
        @Autowired
        ProfileRepository profileRepository;

        @Autowired
        MessageService messageService;

        @Test
        void When_ProfileContentValidData_Expect_SuccessUpdateAndReturnUpdatedProfile() {
            String name = "New name";
            Profile profile = profileRepository.findByUuid(TestConst.UUID).get();
            profile.setName(name);

            SQLStatementCountValidator.reset();
            profileRepository.save(profile);
            assertUpdateCount(1);
            assertSelectCount(1);

            Profile updatedProfile = profileRepository.findByUuid(TestConst.UUID).get();
            assertEquals(name, updatedProfile.getName());
        }

        @Test
        void When_ProfileContentInvalidData_Expect_ThrowTransactionSystemExceptionCausedByConstraintViolationException() { ;
            Profile profile = profileRepository.findByUuid(TestConst.UUID).get();
            profile.setEmail("not valid EMAIL");

            TransactionSystemException throwable = assertThrows(TransactionSystemException.class, () -> profileRepository.save(profile));
            assertEquals(ConstraintViolationException.class, throwable.getRootCause().getClass());
            ConstraintViolationException cause = (ConstraintViolationException) throwable.getRootCause();

            for(ConstraintViolation violation: cause.getConstraintViolations()) {
                String messageTemplate = violation.getMessageTemplate();
                String message = messageService.get(messageTemplate.substring(1, messageTemplate.length() - 1));
                assertEquals(message, violation.getMessage());
            }
        }
    }
}
