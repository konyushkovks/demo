package demo.konyushkov.model;

import demo.konyushkov.tools.ProfileHelper;
import demo.konyushkov.tools.ValidatorTest;
import demo.konyushkov.validation.groups.ProfileGroups;
import org.junit.jupiter.api.*;

import java.time.LocalDate;

class ProfileValidationTest {

    private static ValidatorTest<Profile> validator;
    Profile profile;

    @BeforeAll
    public static void setUp() {
        validator = new ValidatorTest<>();
    }

    @BeforeEach
    void init() {
        profile = ProfileHelper.getDefaultProfile();
    }

    @Nested
    @DisplayName("Validation in Default Group")
    public class DefaultGroup {

        @Nested
        public class Success {
            @Test
            public void When_FullyValidProfile_Expect_ViolationsIsEmpty() {
                validator.successValidation(profile);
            }
        }

        @Nested
        public class Failed {
            @AfterEach
            public void test() {
                validator.failedValidation(profile);
            }

            @Test
            public void When_NameInvalidNotBlank_Expect_OneConstraintViolationWithNotBlankMessageTemplate() {
                profile.setName(" ");
                validator.fillTestData("name", "{NotBlank.profile.name}");
            }

            @Test
            public void When_DateInvalidDateBetween_Expect_OneConstraintViolationWithDateBetweenMessageTemplate() {
                profile.setBirthday(LocalDate.MIN);
                validator.fillTestData("birthday", "{DateBetween.profile.birthday}");
            }
        }
    }

    @Nested
    @DisplayName("Validation in ProfilesGroup.Update group")
    public class UpdateGroup {

        @Nested
        public class Success {
            @Test
            public void When_FullyValidProfile_Expect_ViolationsIsEmpty() {
                validator.successValidation(profile, ProfileGroups.Update.class);
            }

            @Test
            public void When_EmailInvalid_Expect_IgnoreEmailAndViolationsIsEmpty() {
                profile.setEmail("invalid_email");
                validator.successValidation(profile, ProfileGroups.Update.class);
            }

            @Test
            public void When_PasswordInvalid_Expect_IgnorePasswordAndViolationsIsEmpty() {
                profile.setPassword("invalid_password");
                validator.successValidation(profile, ProfileGroups.Update.class);
            }
        }

        @Nested
        public class Failed {
            @AfterEach
            public void test() {
                validator.failedValidation(profile);
            }

            @Test
            public void When_NameInvalidNotBlank_Expect_OneConstraintViolationWithNotBlankMessageTemplate() {
                profile.setName(" ");
                validator.fillTestData("name", "{NotBlank.profile.name}");
            }

            @Test
            public void When_DateInvalidDateBetween_Expect_OneConstraintViolationWithDateBetweenMessageTemplate() {
                profile.setBirthday(LocalDate.MIN);
                validator.fillTestData("birthday", "{DateBetween.profile.birthday}");
            }
        }
    }
}
