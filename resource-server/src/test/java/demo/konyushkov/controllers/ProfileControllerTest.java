package demo.konyushkov.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import demo.konyushkov.config.IntegrationTestConfig;
import demo.konyushkov.model.Profile;
import demo.konyushkov.service.MessageService;
import demo.konyushkov.service.ProfileService;
import demo.konyushkov.tools.OAuth2Helper;
import demo.konyushkov.tools.TestConst;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ProfileControllerTest {

    /* Authorization server should be start */

    @Nested
    public class GetProfile extends IntegrationTestConfig {

        @Autowired
        private WebApplicationContext context;

        @Autowired
        private OAuth2Helper oAuth2Helper;

        private MockMvc mockMvc;


        @BeforeEach
        void setup() {
            mockMvc = MockMvcBuilders
                    .webAppContextSetup(context)
                    .apply(springSecurity())
                    .build();
        }


        @Test
        void When_Authorized_Expect_ReturnProfile() throws Exception {
            String accessToken = oAuth2Helper.obtainAccessToken(TestConst.EMAIL, TestConst.PASSWORD);
            System.out.println(accessToken);
            mockMvc.perform(get("/profile")
                    .header("Authorization", "Bearer " + accessToken))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.email").value(TestConst.EMAIL)) // Correct String serialize;
                    .andExpect(jsonPath("$.birthday").value("1996-01-27")); // Correct LocalDate serialize;
        }

        @Test
        void  When_WrongToken_Expect_StatusIsUnauthorized() throws Exception {
            mockMvc.perform(get("/profile")
                    .header("Authorization", "Bearer wrongToken"))
                    .andExpect(status().isUnauthorized());
        }
    }

    @Nested
    public class UpdateProfile extends IntegrationTestConfig {
        @Autowired
        private WebApplicationContext context;

        @Autowired
        private OAuth2Helper oAuth2Helper;

        @Autowired
        ProfileService profileService;

        @Autowired
        MessageService messageService;

        @Autowired
        MappingJackson2HttpMessageConverter converter;

        private MockMvc mockMvc;

        @BeforeEach
        void setup() {
            mockMvc = MockMvcBuilders
                    .webAppContextSetup(context)
                    .apply(springSecurity())
                    .build();
        }


        @Test
        void When_ValidProfile_Expect_SuccessUpdateAndReturnUpdatedProfile() throws Exception {
            String accessToken = oAuth2Helper.obtainAccessToken(TestConst.EMAIL, TestConst.PASSWORD);
            Profile profile = profileService.findByUuid(TestConst.UUID);
            profile.setName(" Updated");

            mockMvc.perform(post("/profile/personal/update")
                    .header("Authorization", "Bearer " + accessToken)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(getJson(profile)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.notice.title").value(messageService.get("ProfileController.update.notice.title")))
                    .andExpect(jsonPath("$.notice.message").value(messageService.get("ProfileController.update.notice.message")))
                    .andExpect(jsonPath("$.payload.name").value(profile.getName().trim()))
                    .andDo(print());
        }

        @Test
        void When_NotValidProfile_Expect_ReturnApiErrorWithViolations() throws Exception {
            String accessToken = oAuth2Helper.obtainAccessToken(TestConst.EMAIL, TestConst.PASSWORD);
            Profile profile = profileService.findByUuid(TestConst.UUID);
            profile.setName("");

            mockMvc.perform(post("/profile/personal/update")
                    .header("Authorization", "Bearer " + accessToken)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(getJson(profile)))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.errors.name").isArray())
                    .andExpect(jsonPath("$.errors.name[0]").value(messageService.get("NotBlank.profile.name")))
                    .andDo(print());
        }

        private String getJson(Object obj) throws JsonProcessingException {
            return converter.getObjectMapper().writeValueAsString(obj);
        }
    }
}
