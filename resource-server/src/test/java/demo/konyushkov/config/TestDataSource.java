package demo.konyushkov.config;


import net.ttddyy.dsproxy.listener.logging.SLF4JLogLevel;
import net.ttddyy.dsproxy.support.ProxyDataSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@Configuration
public class TestDataSource extends DataSourceConfig {

    @Autowired
    public TestDataSource(Environment environment) {
        super(environment);
    }


    @Override
    public DataSource dataSource() {
        ProxyDataSourceBuilder builder = new ProxyDataSourceBuilder();
        builder.logQueryBySlf4j(SLF4JLogLevel.INFO)
                .dataSource(super.dataSource()).countQuery();

        return builder.build();
    }
}
