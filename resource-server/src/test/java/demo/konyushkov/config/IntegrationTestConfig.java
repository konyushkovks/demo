package demo.konyushkov.config;

import demo.konyushkov.tools.OAuth2Helper;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { AppConfig.class, OAuth2Helper.class,
        TestDataSource.class, TestPropertySource.class})
@WebAppConfiguration
@ActiveProfiles({"default", "test", "integration"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Sql(scripts = {
        "classpath:drop_schema.sql",
        "classpath:schema.sql",
        "classpath:data.sql"})
public class IntegrationTestConfig {
}
