package demo.konyushkov.service;

import demo.konyushkov.exception.NotFoundException;
import demo.konyushkov.model.Profile;
import demo.konyushkov.repository.ProfileRepository;
import demo.konyushkov.tools.ProfileHelper;
import demo.konyushkov.tools.TestConst;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import javax.validation.ConstraintViolationException;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class ProfileServiceImplTest {

    private final String PROFILE_NOT_FOUND_CODE = "ProfileService.findByUuid.notFound";

    private ProfileServiceImpl profileService;
    @Mock private ProfileRepository profileRepository;
    @Mock private MessageService messageService;

    @BeforeEach
    void init() {
        profileService = new ProfileServiceImpl(profileRepository, messageService);
        Mockito.when(messageService.get(PROFILE_NOT_FOUND_CODE)).thenReturn(PROFILE_NOT_FOUND_CODE);
    }

    @Nested
    public class FindByUuid {
        @Test
        public void When_ProfileWithCurrentUuidExistInDb_Expect_ReturnProfile() {
            Profile profile = ProfileHelper.getDefaultProfile();
            Mockito.when(profileRepository.findByUuid(TestConst.UUID)).thenReturn(Optional.of(profile));
            assertEquals(profile, profileService.findByUuid(TestConst.UUID), "Should return Profile by uuid");
        }

        @Test
        public void When_ProfileWithCurrentUuidMissingInDb_Expect_ThrowNotFoundException() {
            UUID uuid = UUID.randomUUID();
            Mockito.when(profileRepository.findByUuid(uuid)).thenReturn(Optional.empty());
            Throwable throwable = assertThrows(NotFoundException.class, () -> profileService.findByUuid(uuid));
            assertEquals(PROFILE_NOT_FOUND_CODE, throwable.getLocalizedMessage());
        }
    }

    @Nested
    public class Update {
        Profile input;
        Profile immutableModel;
        Profile updatableModel;

        @BeforeEach
        void init() {
            input = ProfileHelper.getChangedProfile();
            updatableModel = ProfileHelper.getDefaultProfile();
            immutableModel = ProfileHelper.getDefaultProfile();

            Mockito.when(profileRepository.findByUuid(TestConst.UUID)).thenReturn(Optional.of(updatableModel));
            Mockito.when(profileRepository.save(updatableModel)).thenReturn(updatableModel);

            profileService.update(input);
        }

        @Test
        public void When_ChangeUuid_Expect_SuccessUpdateButIgnoreTheChangeOfUuidAndReturnUpdatedProfile() {
            assertEquals(immutableModel.getUuid(), updatableModel.getUuid(), "UUID should be equals before Update and After");
        }

        @Test
        public void When_ChangePassword_Expect_SuccessUpdateButIgnoreTheChangeOfPasswordAndReturnUpdatedProfile() {
            assertEquals(immutableModel.getPassword(), updatableModel.getPassword(), "PASSWORD_HASH should be ignore on update");
        }

        @Test
        public void When_ChangeEmail_Expect_SuccessUpdateButIgnoreTheChangeOfEmailAndReturnUpdatedProfile() {
           assertEquals(immutableModel.getEmail(), updatableModel.getEmail(), "EMAIL should be ignore on update");
        }

        @Test
        public void When_ChangeMobilePhone_Expect_SuccessUpdateButIgnoreTheChangeOfMobilePhoneAndReturnUpdatedProfile() {
            assertEquals(immutableModel.getMobilePhone(), updatableModel.getMobilePhone(), "mobile should be ignore on update");
        }

        @Test
        public void When_ChangeName_Expect_SuccessUpdateAndReturnUpdatedProfile() {
            assertEquals(input.getName(), updatableModel.getName(), "should update name");
        }

        @Test
        public void When_ChangeSurname_Expect_SuccessUpdateAndReturnUpdatedProfile() {
           assertEquals(input.getSurname(), updatableModel.getSurname(), "should update surname");
        }


        @Test
        public void When_ChangePhoto_Expect_SuccessUpdateAndReturnUpdatedProfile() {
            assertEquals(input.getPhoto(), updatableModel.getPhoto(), "should update photo");
        }

        @Test
        public void When_ChangeBirthday_Expect_SuccessUpdateAndReturnUpdatedProfile() {
            assertEquals(input.getBirthday(), updatableModel.getBirthday(), "should update birthday");
        }

        @Test
        public void When_ChangeNote_Expect_SuccessUpdateAndReturnUpdatedProfile() {
            assertEquals(input.getNote(), updatableModel.getNote(), "should update note");
        }

        @Test
        public void When_ChangeGender_Expect_SuccessUpdateAndReturnUpdatedProfile() {
            assertEquals(input.getGender(), updatableModel.getGender(), "should update gender");
        }

        @Test
        public void When_TryUpdateProfileWhichNotExistInDB_Expect_ThrowNotFoundException() {
            Mockito.doThrow(new NotFoundException(messageService.get(PROFILE_NOT_FOUND_CODE))).when(profileRepository).findByUuid(TestConst.UUID);
            Throwable throwable = assertThrows(NotFoundException.class, () -> profileService.update(ProfileHelper.getChangedProfile()));
            assertEquals(PROFILE_NOT_FOUND_CODE, throwable.getLocalizedMessage());
        }

        @Test
        public void When_TryUpdateProfileFilledInvalidData_Expect_ThrowConstraintViolationException() {
            Mockito.doThrow(new ConstraintViolationException("Constraint violation exception", null)).when(profileRepository).findByUuid(TestConst.UUID);
            Throwable throwable = assertThrows(ConstraintViolationException.class, () -> profileService.update(ProfileHelper.getChangedProfile()));
            assertEquals("Constraint violation exception", throwable.getLocalizedMessage());
        }
    }
}
