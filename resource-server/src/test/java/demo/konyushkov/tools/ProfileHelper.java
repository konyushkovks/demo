package demo.konyushkov.tools;

import demo.konyushkov.model.Gender;
import demo.konyushkov.model.Profile;

import java.time.LocalDate;

public class ProfileHelper {
    public static Profile getDefaultProfile() {
        Profile profile = new Profile();
        profile.setUuid(TestConst.UUID);
        profile.setName("Кирилл");
        profile.setSurname("Конюшков");
        profile.setPhoto("/img/photo.jpg");
        profile.setEmail(TestConst.EMAIL);
        profile.setMobilePhone("8 (985) 412 44 37");
        profile.setPassword(TestConst.PASSWORD_HASH);
        profile.setGender(Gender.MALE);
        profile.setNote("note");
        profile.setBirthday(LocalDate.parse("1996-01-27"));
        return profile;
    }

    public static Profile getChangedProfile() {
        Profile profile = new Profile();
        profile.setUuid(TestConst.UUID);
        profile.setName("NEW NAME");
        profile.setSurname("NEW SURNAME");
        profile.setPhoto("NEW PHOTO");
        profile.setEmail("NEW EMAIL");
        profile.setMobilePhone("NEW PHONE");
        profile.setPassword("NEW PASSWORD_HASH");
        profile.setGender(Gender.FEMALE);
        profile.setNote("NEW NOTE");
        profile.setBirthday(LocalDate.parse("1998-01-27"));

        return profile;
    }

}
