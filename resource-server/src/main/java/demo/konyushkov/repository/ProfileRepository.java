package demo.konyushkov.repository;

import demo.konyushkov.model.Profile;
import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.UUID;

public interface ProfileRepository extends Repository<Profile, UUID> {

    Optional<Profile> findByUuid(UUID uuid);
    Profile save(Profile profile);
}
