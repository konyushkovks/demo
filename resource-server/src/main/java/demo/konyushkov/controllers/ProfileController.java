package demo.konyushkov.controllers;

import demo.konyushkov.dto.ApiResponse;
import demo.konyushkov.dto.Notice;
import demo.konyushkov.helper.AuthenticationHelper;
import demo.konyushkov.model.Profile;
import demo.konyushkov.service.MessageService;
import demo.konyushkov.service.ProfileService;
import demo.konyushkov.validation.groups.ProfileGroups;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProfileController {
    private final ProfileService profileService;
    private final MessageService messageService;

    @Autowired
    public ProfileController(ProfileService profileService, MessageService messageService) {
        this.profileService = profileService;
        this.messageService = messageService;
    }

    @GetMapping("/profile")
    public Profile get(OAuth2Authentication authentication) {
        return profileService.findByUuid(AuthenticationHelper.getUUID(authentication));
    }

    @PostMapping(value = "/profile/personal/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse<Profile> update(@RequestBody @Validated(ProfileGroups.Update.class) Profile profile,
                                              OAuth2Authentication authentication) {

        profile.setUuid(AuthenticationHelper.getUUID(authentication));

        return new ApiResponse<>(new Notice(messageService.get("ProfileController.update.notice.title"),
                                            messageService.get("ProfileController.update.notice.message")),
                profileService.update(profile));
    }
}
