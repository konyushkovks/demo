package demo.konyushkov.model;

import demo.konyushkov.validation.constraints.DateBetween;
import demo.konyushkov.validation.groups.ProfileGroups;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;
import java.time.LocalDate;

@Entity
@Getter @Setter
@DiscriminatorValue("null")
public class Profile extends AbstractUser {

    /* Основная информация */
    @NotBlank(message = "{NotBlank.profile.name}",
                groups = { Default.class, ProfileGroups.Update.class })
    @Column(nullable = false)
    private String name;
    private String surname;
    private String photo;
    @DateBetween(min = "1950-01-01", max = "2004-12-31",
                message = "{DateBetween.profile.birthday}",
                groups = { Default.class, ProfileGroups.Update.class })
    private LocalDate birthday;
    @Column(columnDefinition = "clob")
    private String note;
    @Enumerated(EnumType.ORDINAL)
    @Column(columnDefinition = "smallint")
    private Gender gender;

    /* Контактная информация */
    @Column(name = "mobile_phone")
    private String mobilePhone;
}
