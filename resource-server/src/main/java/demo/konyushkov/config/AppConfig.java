package demo.konyushkov.config;

import demo.konyushkov.security.config.SecurityConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackages = {"demo.konyushkov"})
@Import({HibernateConfig.class, SecurityConfig.class})
public class AppConfig extends AbstractWebMvcConfig  {
}
