package demo.konyushkov.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.sql.DataSource;

@Configuration
@Import(DataSourceConfig.class)
public class HibernateConfig extends AbstractHibernateConfig {

    @Autowired
    public HibernateConfig(Environment environment, DataSource dataSource, LocalValidatorFactoryBean validator) {
        super(environment, dataSource, validator);
    }
}
