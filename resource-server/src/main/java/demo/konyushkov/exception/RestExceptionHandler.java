package demo.konyushkov.exception;


import demo.konyushkov.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class RestExceptionHandler extends AbstractRestExceptionHandler {

    @Autowired
    public RestExceptionHandler(MessageService messageService) {
        super(messageService);
    }
}
