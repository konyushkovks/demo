package demo.konyushkov.validation.groups;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProfileGroups {
    public interface Update {}
}
