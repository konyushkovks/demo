package demo.konyushkov.service;

import demo.konyushkov.exception.NotFoundException;
import demo.konyushkov.model.Profile;
import demo.konyushkov.repository.ProfileRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
public class ProfileServiceImpl implements ProfileService {

    private final ProfileRepository profileRepository;
    private final MessageService messageService;

    @Autowired
    public ProfileServiceImpl(ProfileRepository repository, MessageService messageService) {
        this.profileRepository = repository;
        this.messageService = messageService;
    }

    @Override
    public Profile findByUuid(UUID uuid) {
        Optional<Profile> user = profileRepository.findByUuid(uuid);

        return user.orElseThrow(() -> new NotFoundException(messageService.get("ProfileService.findByUuid.notFound")));
    }

    @Override
    @Transactional
    public Profile update(Profile input) {
        Profile model = findByUuid(input.getUuid());
        BeanUtils.copyProperties(input, model, "mobilePhone", "email", "password");

        return profileRepository.save(model);
    }
}
