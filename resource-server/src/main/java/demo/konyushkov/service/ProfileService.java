package demo.konyushkov.service;

import demo.konyushkov.model.Profile;

import java.util.UUID;

public interface ProfileService {
    Profile findByUuid(UUID uuid);
    Profile update(Profile profile);
}
