package demo.konyushkov.controller;

import demo.konyushkov.captcha.CaptchaLogin;
import demo.konyushkov.captcha.CaptchaService;
import demo.konyushkov.helper.AuthenticationHelper;
import demo.konyushkov.helper.HttpHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller
public class AuthController {

    @Value("${app.vue_client.url}")
    private String vueClientUrl;
    private final CaptchaService loginCaptchaService;

    @Autowired
    public AuthController(@CaptchaLogin CaptchaService loginCaptchaService) {
        this.loginCaptchaService = loginCaptchaService;
    }

    @RequestMapping(value="/login", method = RequestMethod.GET)
    public String loginPage(Model model) {
        if(!AuthenticationHelper.isAnonymous(SecurityContextHolder.getContext())) {
            log.info("Redirect to {}", vueClientUrl);
            return "redirect:" + vueClientUrl;
        }

        model.addAttribute("captchaSiteKey", loginCaptchaService.getReCaptchaSite());
        model.addAttribute("isShowCaptcha", loginCaptchaService.isCaptchaShow());

        return "login";
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutProcess (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = AuthenticationHelper.getAuthentication(SecurityContextHolder.getContext());

        if(HttpHelper.isRequestRefererUrl(request, vueClientUrl)) {
            log.info("User {} logout", auth.getName());
            new SecurityContextLogoutHandler().logout(request, response, auth);
            return "redirect:/login?logout";
        }
        else {
            log.warn("User {} try logout not from vue client", auth.getName());
            return "redirect:" + vueClientUrl;
        }
    }
}
