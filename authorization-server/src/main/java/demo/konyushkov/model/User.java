package demo.konyushkov.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Set;

@Entity
@DiscriminatorValue("null")
@Getter @Setter
public class User extends AbstractUser {

    @Fetch(FetchMode.SUBSELECT)
    @ElementCollection(targetClass = AuthoritiesEnum.class, fetch = FetchType.EAGER)
    @CollectionTable(name="users_authorities",
            joinColumns = { @JoinColumn(name = "user_uuid") })
    @Column(name="authority_name")
    @Enumerated(EnumType.STRING)
    private Set<AuthoritiesEnum> authorities;
}
