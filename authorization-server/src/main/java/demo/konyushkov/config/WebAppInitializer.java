package demo.konyushkov.config;



public class WebAppInitializer extends AbstractWebAppInitializer{

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] { AppConfig.class };
    }
}


