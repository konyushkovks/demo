package demo.konyushkov.config;

import demo.konyushkov.captcha.CaptchaConfiguration;
import demo.konyushkov.security.config.SecurityConfig;
import demo.konyushkov.service.AttemptService;
import demo.konyushkov.service.AttemptServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableAsync
@Import({CaptchaConfiguration.class,
        HibernateConfig.class,
        WebRequestContextListener.class,
        SecurityConfig.class
})
public class AppConfig extends AbstractWebMvcConfig  {

    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");

        return viewResolver;
    }

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/css/**").addResourceLocations("/resources/css/");
        registry.addResourceHandler("/js/**").addResourceLocations("/resources/js/");
    }

    @Bean
    public RestOperations restTemplate() {
        return new RestTemplate();
    }

    @Bean
    @Login
    public AttemptService loginAttemptService() {
        return new AttemptServiceImpl(10, 1, TimeUnit.DAYS);
    }
}
