package demo.konyushkov.captcha;

import demo.konyushkov.config.Login;
import demo.konyushkov.helper.HttpHelper;
import demo.konyushkov.service.AttemptService;
import demo.konyushkov.service.AttemptServiceImpl;
import demo.konyushkov.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestOperations;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

@Configuration
public class CaptchaConfiguration {

    private final HttpServletRequest request;
    private final RestOperations restOperations;
    private final AttemptService loginAttemptService;
    private final MessageService messageService;

    @Value("${google.recaptcha.key.site}")
    private String CAPTCHA_KEY;

    @Value("${google.recaptcha.key.secret}")
    private String CAPTCHA_SECRET;

    @Autowired
    public CaptchaConfiguration(RestOperations restOperations,
                                @Login AttemptService loginAttemptService,
                                MessageService messageService, HttpServletRequest request) {
        this.restOperations = restOperations;
        this.loginAttemptService = loginAttemptService;
        this.messageService = messageService;
        this.request = request;
    }

    @Bean
    public CaptchaSettings captchaSettings() {
        return new CaptchaSettings(CAPTCHA_KEY, CAPTCHA_SECRET);
    }

    @Bean
    @CaptchaLogin
    public AttemptService captchaAttemptService() {
        return new AttemptServiceImpl(4, 10, TimeUnit.HOURS);
    }

    @Bean
    @CaptchaLogin
    public CaptchaService loginCaptchaService() {
        return new CaptchaServiceImpl(
                captchaSettings(),
                captchaAttemptService(),
                messageService,
                restOperations,
                () -> loginAttemptService.getAttempts(HttpHelper.getClientIP(request)) >= 3);
    }
}
