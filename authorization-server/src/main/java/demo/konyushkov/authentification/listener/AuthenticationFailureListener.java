package demo.konyushkov.authentification.listener;

import demo.konyushkov.config.Login;
import demo.konyushkov.helper.HttpHelper;
import demo.konyushkov.service.AttemptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class AuthenticationFailureListener
  implements ApplicationListener<AbstractAuthenticationFailureEvent> {

    private final HttpServletRequest request;
    private final AttemptService loginAttemptService;

    @Autowired
    public AuthenticationFailureListener(@Login AttemptService loginAttemptService,
                                         HttpServletRequest request) {
        this.loginAttemptService = loginAttemptService;
        this.request = request;
    }

    @Override
    public void onApplicationEvent(AbstractAuthenticationFailureEvent e) {
        loginAttemptService.failed(HttpHelper.getClientIP(request));
    }
}
