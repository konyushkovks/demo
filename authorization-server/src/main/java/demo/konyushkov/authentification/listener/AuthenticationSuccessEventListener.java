package demo.konyushkov.authentification.listener;

import demo.konyushkov.config.Login;
import demo.konyushkov.helper.HttpHelper;
import demo.konyushkov.service.AttemptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class AuthenticationSuccessEventListener
  implements ApplicationListener<AuthenticationSuccessEvent> {


    private final HttpServletRequest request;
    private final AttemptService loginAttemptService;

    @Autowired
    public AuthenticationSuccessEventListener(@Login AttemptService loginAttemptService, HttpServletRequest request) {
        this.loginAttemptService = loginAttemptService;
        this.request = request;
    }

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent e) {
        loginAttemptService.succeeded(HttpHelper.getClientIP(request));
    }
}
