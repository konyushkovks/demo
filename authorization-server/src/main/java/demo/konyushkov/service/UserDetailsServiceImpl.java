package demo.konyushkov.service;

import demo.konyushkov.captcha.CaptchaLogin;
import demo.konyushkov.captcha.CaptchaService;
import demo.konyushkov.config.Login;
import demo.konyushkov.helper.HttpHelper;
import demo.konyushkov.principal.UserPrincipal;
import demo.konyushkov.repository.UserDetailsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserDetailsRepository userDetailsRepository;
    private final AttemptService loginAttemptService;
    private final CaptchaService loginCaptchaService;
    private final MessageService messageService;
    private final HttpServletRequest request;

    @Autowired
    public UserDetailsServiceImpl(UserDetailsRepository repository,
                                  @Login AttemptService loginAttemptService,
                                  @CaptchaLogin CaptchaService loginCaptchaService,
                                  MessageService messageService, HttpServletRequest request) {
        this.userDetailsRepository = repository;
        this.loginAttemptService = loginAttemptService;
        this.loginCaptchaService = loginCaptchaService;
        this.messageService = messageService;
        this.request = request;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        log.info(String.format("Loading user by email: %s", username));

        final String ip = HttpHelper.getClientIP(request);

        if (loginAttemptService.isBlocked(ip)) {
            throw new RuntimeException(messageService.get("LoginAttemptService.blocked",
                    "Client exceeded maximum number of failed attempts"));
        }

        if(loginCaptchaService.isCaptchaShow()) {
            loginCaptchaService.processResponse(request);
        }

        return userDetailsRepository.findByEmail(username)
                .map(UserPrincipal::new)
                .orElseThrow(() -> new UsernameNotFoundException(messageService
                        .get("AbstractUserDetailsAuthenticationProvider.badCredentials",
                                "Wrong email or password")));
    }
}
