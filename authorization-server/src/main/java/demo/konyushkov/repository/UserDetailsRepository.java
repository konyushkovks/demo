package demo.konyushkov.repository;

import demo.konyushkov.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface UserDetailsRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);
}
