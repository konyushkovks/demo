"use strict";
const webpack = require("webpack");
const baseConfig = require("./webpack.base.config");
const merge = require("webpack-merge");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const WriteFilePlugin = require("write-file-webpack-plugin");
const config = merge(baseConfig, {
    mode: "development",

    devtool: "#cheap-module-source-map",

    devServer: {
        hot: true,
        publicPath: "/",
        watchOptions: {
            poll: true
        },
        port: 8001,
        host: "localhost"
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new WriteFilePlugin(),
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: "./src/index.html",
            inject: true
        })
    ]
});

module.exports = config;
