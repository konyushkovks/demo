<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>Login Page</title>
    <link rel="stylesheet" href="/css/0.styles.css">
    <script src="https://www.google.com/recaptcha/api.js?hl=ru&onload=vueRecaptchaApiLoaded&render=explicit" async defer></script>
</head>
<body>
    <div id="app"></div>
    <input type="hidden" class="js-crsf" name="${_csrf.parameterName}" value="${_csrf.token}" />
    <c:if test="${not empty param.error && not empty SPRING_SECURITY_LAST_EXCEPTION }">
        <input type="hidden"  class="js-auth-error" name="authError" value="${ SPRING_SECURITY_LAST_EXCEPTION }" />
    </c:if>
    <c:if test="${isShowCaptcha == true}">
        <input type="hidden" class="js-captcha" name="captcha" value="${captchaSiteKey}"/>
    </c:if>
    <script type="text/javascript" src="/js/css/styles.js"></script>
    <script type="text/javascript" src="/js/vendors.js"></script>
    <script type="text/javascript" src="/js/app.js"></script>
</body>
</html>
