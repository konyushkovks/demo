package demo.konyushkov.principal;

import demo.konyushkov.model.AuthoritiesEnum;
import demo.konyushkov.model.User;
import demo.konyushkov.tools.TestConst;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class UserPrincipalUnitTest {

    private User user;

    @BeforeEach
    void init() {
        user = new User();
        user.setUuid(TestConst.UUID);
        user.setEmail(TestConst.EMAIL);
        user.setAuthorities(new HashSet<>(Arrays.asList(AuthoritiesEnum.ROLE_ADMIN, AuthoritiesEnum.ROLE_MODERATOR, AuthoritiesEnum.ROLE_SUPERUSER)));
        user.setPassword(TestConst.PASSWORD_HASH);
    }

    @Nested
    public class GetAuthorities {
        @Test
        void When_UserHasAuthorities_Expect_ReturnListOfAuthoritiesEnum() {
            UserPrincipal userPrincipal = new UserPrincipal(user);
            Collection<? extends GrantedAuthority>  authorities = userPrincipal.getAuthorities();
            assertEquals(3, authorities.size());
        }
    }

    @Nested
    public class GetUserName {
        @Test
        void When_UserPrincipalsFilled_Expect_ReturnUserUuid() {
            UserPrincipal userPrincipal = new UserPrincipal(user);
            assertEquals(user.getUuid().toString(), userPrincipal.getUsername());
        }
    }

    @Nested
    public class GetUserPassword {
        @Test
        void When_UserPrincipalsFilled_Expect_ReturnUserPassword() {
            UserPrincipal userPrincipal = new UserPrincipal(user);
            assertEquals(user.getPassword(), userPrincipal.getPassword());
        }
    }
}
