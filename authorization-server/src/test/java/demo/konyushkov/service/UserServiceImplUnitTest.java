package demo.konyushkov.service;


import demo.konyushkov.captcha.CaptchaService;
import demo.konyushkov.helper.HttpHelper;
import demo.konyushkov.model.User;
import demo.konyushkov.principal.UserPrincipal;
import demo.konyushkov.repository.UserDetailsRepository;
import demo.konyushkov.tools.TestConst;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UserServiceImplUnitTest {

    private UserDetailsService userDetailsService;
    @Mock AttemptService loginAttemptService;
    @Mock UserDetailsRepository userDetailsRepository;
    @Mock CaptchaService loginCaptchaService;
    @Mock MessageService messageService;
    @Mock HttpServletRequest request;
    private final String blockedCode = "LoginAttemptService.blocked";
    private final String notFoundCode = "AbstractUserDetailsAuthenticationProvider.badCredentials";
    private final String captchaResponse = "captchaResponse";
    private final User user = new User();

    @BeforeEach
    void init() {
        userDetailsService = new UserDetailsServiceImpl(userDetailsRepository,
                loginAttemptService, loginCaptchaService, messageService, request);

        Mockito.when(HttpHelper.getClientIP(request)).thenReturn(TestConst.IP);
        Mockito.when(messageService.get(eq(blockedCode), anyString())).thenReturn(blockedCode);
        Mockito.when(messageService.get(eq(notFoundCode), anyString())).thenReturn(notFoundCode);
        Mockito.when(HttpHelper.getReCaptchaResponse(request)).thenReturn(captchaResponse);

        user.setEmail(TestConst.EMAIL);
        user.setUuid(TestConst.UUID);
    }

    @Nested
    class LoadUserByUserName {

        @Test
        void When_CorrectCredentialsAndCaptchaHide_Expect_ReturnUserPrincipal() {
            Mockito.when(loginAttemptService.isBlocked(TestConst.IP)).thenReturn(false);
            Mockito.when(loginCaptchaService.isCaptchaShow()).thenReturn(false);
            Mockito.when(userDetailsRepository.findByEmail(TestConst.EMAIL)).thenReturn(Optional.of(user));

            UserPrincipal principal = (UserPrincipal) userDetailsService.loadUserByUsername(TestConst.EMAIL);
            assertNotNull(principal, "Find single user by EMAIL");
            assertEquals(user, principal.getUser(), "Return correct object");
        }

        @Test
        void When_CorrectCredentialsAndCorrectCaptchaValidation_Expect_ReturnUserPrincipal() {
            Mockito.when(loginAttemptService.isBlocked(TestConst.IP)).thenReturn(false);
            Mockito.when(loginCaptchaService.isCaptchaShow()).thenReturn(true);
            Mockito.doNothing().when(loginCaptchaService).processResponse(request);
            Mockito.when(userDetailsRepository.findByEmail(TestConst.EMAIL)).thenReturn(Optional.of(user));

            UserPrincipal principal = (UserPrincipal) userDetailsService.loadUserByUsername(TestConst.EMAIL);
            assertNotNull(principal, "Find single user by EMAIL");
            assertEquals(user, principal.getUser(), "Return correct object");
        }

        @Test
        void When_ClientBlockByIpInLoginAttemptService_Expect_ThrowRuntimeExceptionWithMessageReportAboutBlock() {
            Mockito.when(loginAttemptService.isBlocked(TestConst.IP)).thenReturn(true);
            Throwable exception = assertThrows(RuntimeException.class, () -> userDetailsService.loadUserByUsername(TestConst.EMAIL));
            assertEquals(blockedCode, exception.getMessage());
        }

        @Test
        void When_ClientBlockByIpInLoginAttemptService_Expect_IgnoreCaptchaValidation() {
            Mockito.when(loginAttemptService.isBlocked(TestConst.IP)).thenReturn(true);
            Mockito.when(loginCaptchaService.isCaptchaShow()).thenReturn(true);
            Mockito.doThrow(RuntimeException.class).when(loginCaptchaService).processResponse(request);

            Throwable exception = assertThrows(RuntimeException.class, () -> userDetailsService.loadUserByUsername(TestConst.EMAIL));
            assertEquals(blockedCode, exception.getMessage());
        }

        @Test
        void When_CaptchaValidationNotSuccess_Expect_ThrowRuntimeException() {
            Mockito.when(loginAttemptService.isBlocked(TestConst.IP)).thenReturn(false);
            Mockito.when(loginCaptchaService.isCaptchaShow()).thenReturn(true);
            Mockito.doThrow(RuntimeException.class).when(loginCaptchaService).processResponse(request);

            assertThrows(RuntimeException.class, () -> userDetailsService.loadUserByUsername(TestConst.EMAIL));
        }

        @Test
        void When_UserNotFoundByEmail_Expect_ThrowUsernameNotFoundException() {
            Mockito.when(loginAttemptService.isBlocked(TestConst.IP)).thenReturn(false);
            Mockito.when(loginCaptchaService.isCaptchaShow()).thenReturn(false);

            Throwable exception = assertThrows(UsernameNotFoundException.class, () ->
                    userDetailsService.loadUserByUsername(TestConst.MISSING_EMAIL)
            );

            assertEquals(notFoundCode, exception.getMessage());
        }
    }
}
