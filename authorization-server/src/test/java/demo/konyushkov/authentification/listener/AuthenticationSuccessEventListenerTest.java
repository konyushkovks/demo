package demo.konyushkov.authentification.listener;

import demo.konyushkov.helper.HttpHelper;
import demo.konyushkov.service.AttemptService;
import demo.konyushkov.tools.TestConst;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import javax.servlet.http.HttpServletRequest;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class AuthenticationSuccessEventListenerTest {

    private AuthenticationSuccessEventListener asListener;
    @Mock AttemptService loginAttemptService;
    @Mock HttpServletRequest request;
    private int failedAttempts = 0;

    @BeforeEach
    void init() {
        this.asListener = new AuthenticationSuccessEventListener(loginAttemptService, request);
        Mockito.when(HttpHelper.getClientIP(request)).thenReturn(TestConst.IP);
        Mockito.doAnswer((i) -> failedAttempts = 0).when(loginAttemptService).succeeded(TestConst.IP);
    }

    @Test
    void When_OnApplicationEventTriggered_Expect_InvalidateFailedAttempts() {
        failedAttempts = 2;
        asListener.onApplicationEvent(null);
        assertEquals(0, failedAttempts, "Failed attempts clear");
    }
}
