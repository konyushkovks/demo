package demo.konyushkov.controllers;

import demo.konyushkov.captcha.CaptchaLogin;
import demo.konyushkov.captcha.CaptchaService;
import demo.konyushkov.config.IntegrationTestConfig;
import demo.konyushkov.config.Login;
import demo.konyushkov.helper.HttpHelper;
import demo.konyushkov.service.AttemptService;
import demo.konyushkov.service.MessageService;
import demo.konyushkov.tools.TestConst;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

import static demo.konyushkov.tools.CustomSecurityMockMvcRequestPostProcessors.admin;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.anonymous;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


class AuthControllerIntegrationTest {

    @Nested
    public class LoginPage extends IntegrationTestConfig {

        @Autowired private WebApplicationContext context;
        @Autowired @Qualifier("loginCaptchaService") private CaptchaService loginCaptchaService;
        @Value("${app.vue_client.url}") private String vueClientUrl;
        private MockMvc mvc;


        @BeforeEach
        void setup() {
            mvc = MockMvcBuilders
                    .webAppContextSetup(context)
                    .apply(springSecurity())
                    .build();
        }

        @Test
        void When_RequestAnonymous_Expect_ShowLoginPage() throws Exception {
            mvc.perform(get("/login").with(anonymous()).param("error","true"))
                    .andExpect(unauthenticated())
                    .andExpect(status().isOk())
                    .andExpect(view().name("login"))
                    .andExpect(model().attribute("isShowCaptcha", loginCaptchaService.isCaptchaShow()))
                    .andExpect(model().attribute("captchaSiteKey", loginCaptchaService.getReCaptchaSite()))
                    .andDo(print());
        }

        @Test
        void When_RequestAuthorized_Expect_RedirectToVueClient() throws Exception {
            mvc.perform(get("/login")
                    .with(admin()))
                    .andExpect(authenticated())
                    .andExpect(status().is3xxRedirection())
                    .andExpect(redirectedUrl(vueClientUrl));
        }
    }

    @Nested
    public class LoginProcess extends IntegrationTestConfig {
        @Autowired private WebApplicationContext context;
        @Autowired @CaptchaLogin private CaptchaService loginCaptchaService;
        @Autowired @Login private AttemptService loginAttemptService;
        @Autowired private MessageService messageService;
        @Autowired private HttpServletRequest request;
        @Value("${app.vue_client.url}") private String vueClientUrl;
        private MockMvc mvc;
        private String ip;

        @BeforeEach
        void setup() {
            mvc = MockMvcBuilders
                    .webAppContextSetup(context)
                    .apply(springSecurity())
                    .build();

            ip = HttpHelper.getClientIP(request);
        }

        @Test
        void When_ValidCredentials_Expect_SuccessLoginAndRedirectToVueClient() throws Exception {
            mvc.perform(post("/j_spring_security_check")
                    .param("username", TestConst.EMAIL)
                    .param("password", TestConst.PASSWORD)
                    .with(csrf()))
                    .andExpect(status().is3xxRedirection())
                    .andExpect(redirectedUrl(vueClientUrl));
        }

        @Test
        void When_CorrectEmailAndWrongPassword_Expect_FailedLoginAndIncrementFailedAttemptsAndRedirectToLoginPageAndShowLoginExceptionMessage() throws Exception {
            loginProcess_failed(TestConst.EMAIL, TestConst.WRONG_PASSWORD);
        }

        @Test
        void When_WrongEmail_Expect_FailedLoginAndIncrementFailedAttemptsAndRedirectToLoginPageAndShowLoginExceptionMessage() throws Exception {
            loginProcess_failed(TestConst.MISSING_EMAIL, TestConst.WRONG_PASSWORD);
        }

        private void loginProcess_failed(String email, String password) throws Exception {
            assertEquals(0, loginAttemptService.getAttempts(ip));
            assertFalse(loginAttemptService.isBlocked(ip));

            MvcResult result = mvc.perform(post("/j_spring_security_check")
                    .param("username", email)
                    .param("password", password)
                    .with(csrf()))
                    .andExpect(status().is3xxRedirection())
                    .andExpect(redirectedUrl("/login?error=true"))
                    .andDo(print()).andReturn();

            String last_exception = Objects
                    .requireNonNull(result.getRequest().getSession())
                    .getAttribute("SPRING_SECURITY_LAST_EXCEPTION")
                    .toString();

            assertEquals(messageService.get("AbstractUserDetailsAuthenticationProvider.badCredentials"), last_exception);
            assertEquals(1, loginAttemptService.getAttempts(ip));
            assertFalse(loginAttemptService.isBlocked(ip));
        }
    }

    @Nested
    public class LogoutProcess extends IntegrationTestConfig {
        @Autowired private WebApplicationContext context;
        @Value("${app.vue_client.url}") private String vueClientUrl;
        private MockMvc mvc;

        @BeforeEach
        void setup() {
            mvc = MockMvcBuilders
                    .webAppContextSetup(context)
                    .apply(springSecurity())
                    .build();
        }

        @Test
        void When_UserLogoutFromVueClient_Expect_SuccessLogoutAndRedirectToLogoutPage() throws Exception {
            mvc.perform(get("/logout")
                    .with(admin())
                    .header("referer", vueClientUrl))
                    .andExpect(status().is3xxRedirection())
                    .andExpect(redirectedUrl("/login?logout"));
        }

        @Test
        void When_UserTryLogoutNotFromVueClient_Expect_RedirectToVueClient() throws Exception {
            mvc.perform(get("/logout")
                    .with(admin()))
                    .andExpect(status().is3xxRedirection())
                    .andExpect(redirectedUrl(vueClientUrl));
        }

        @Test
        void When_AnonymousTryLogout_Expect_RedirectToLoginPage() throws Exception {
            mvc.perform(get("/logout")
                    .with(anonymous()))
                    .andExpect(status().is3xxRedirection())
                    .andExpect(redirectedUrl("http://localhost/login")).andDo(print());
        }
    }
}
