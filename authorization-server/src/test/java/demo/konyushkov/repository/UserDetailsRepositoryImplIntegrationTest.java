package demo.konyushkov.repository;

import com.vladmihalcea.sql.SQLStatementCountValidator;
import demo.konyushkov.config.IntegrationTestConfig;
import demo.konyushkov.model.AuthoritiesEnum;
import demo.konyushkov.model.User;
import demo.konyushkov.tools.TestConst;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static com.vladmihalcea.sql.SQLStatementCountValidator.assertSelectCount;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserDetailsRepositoryImplIntegrationTest  {

    @Nested
    public class FindByEmail extends IntegrationTestConfig {
        @Autowired
        UserDetailsRepository userDetailsRepository;

        @Autowired
        PasswordEncoder passwordEncoder;

        @Test
        void When_UserExistInDb_Expect_ReturnOptionalWithUser() {
            SQLStatementCountValidator.reset();
            Optional<User> optional = userDetailsRepository.findByEmail(TestConst.EMAIL);
            assertSelectCount(2);

            assertTrue(optional.isPresent());
            User user = optional.get();

            assertEquals(TestConst.EMAIL, user.getEmail());
            assertEquals(TestConst.UUID, user.getUuid());
            assertEquals(3, user.getAuthorities().size());
            assertTrue(user.getAuthorities().contains(AuthoritiesEnum.ROLE_ADMIN));
            assertTrue(user.getAuthorities().contains(AuthoritiesEnum.ROLE_SUPERUSER));
            assertTrue(user.getAuthorities().contains(AuthoritiesEnum.ROLE_MODERATOR));
        }
    }
}
