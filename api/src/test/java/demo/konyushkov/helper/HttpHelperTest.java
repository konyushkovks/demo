package demo.konyushkov.helper;

import demo.konyushkov.tools.TestConst;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import javax.servlet.http.HttpServletRequest;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class HttpHelperTest {

    @Mock HttpServletRequest request;

    @Nested
    public class GetClientIP {
        @Test
        void When_XForwarderForHeaderIsMissing_Expect_ReturnRemoteAddr() {
            Mockito.when(request.getHeader("X-Forwarder-For")).thenReturn(null);
            Mockito.when(request.getRemoteAddr()).thenReturn(TestConst.IP);

            assertEquals(TestConst.IP, HttpHelper.getClientIP(request));
        }

        @Test
        void When_XForwarderForHeaderExist_Expect_ReturnXForwarderFor() {
            String[] ips = {"203.0.113.195", "70.41.3.18", "150.172.238.178"};
            Mockito.when(request.getHeader("X-Forwarder-For")).thenReturn(String.format("%s, %s, %s", ips[0], ips[1], ips[2]));
            Mockito.when(request.getRemoteAddr()).thenReturn(TestConst.IP);

            assertEquals(ips[0], HttpHelper.getClientIP(request));
        }
    }

    @Nested
    public class GetRecaptchaResponse {

        @Test
        void When_RequestHasGRecaptchaResponseParameter_Expect_Return_GRecaptchaResponseParameter() {
            String response = "validResponse";
            Mockito.when(request.getParameter("g-recaptcha-response")).thenReturn(response);

            assertEquals(response, HttpHelper.getReCaptchaResponse(request));
        }

        @Test
        void When_RequestNotHasGRecaptchaResponseParameter_Expect_Return_Null() {
            Mockito.when(request.getParameter("g-recaptcha-response")).thenReturn(null);

            assertNull(HttpHelper.getReCaptchaResponse(request));
        }

        @Test
        void When_RequestIsNull_Expect_Return_Null() {
            assertNull(HttpHelper.getReCaptchaResponse(null));
        }
    }

    @Nested
    public class IsRequestRefererUrl {

        @Test
        void When_RequestRefererHeaderStartWithUrl_Expect_True() {
            String url = "http://localhost:8080/random";
            Mockito.when(request.getHeader("referer")).thenReturn(url);

            assertTrue(HttpHelper.isRequestRefererUrl(request, "http://localhost:8080"));
        }

        @Test
        void When_RequestRefererHeaderIsNull_Expect_False() {
            Mockito.when(request.getHeader("referer")).thenReturn(null);

            assertFalse(HttpHelper.isRequestRefererUrl(request, "http://localhost:8080"));
        }

        @Test
        void When_RequestRefererHeaderIsNotStartWithUrl_Expect_False() {
            String url = "http://another-domain/random";
            Mockito.when(request.getHeader("referer")).thenReturn(url);

            assertFalse(HttpHelper.isRequestRefererUrl(request, "http://localhost:8080"));
        }

        @Test
        void When_UrlIsNull_Expect_False() {
            String url = "http://another-domain/random";
            Mockito.when(request.getHeader("referer")).thenReturn(url);

            assertFalse(HttpHelper.isRequestRefererUrl(request, null));
        }


        @Test
        void When_RequstIsNull_Expect_False() {
            assertFalse(HttpHelper.isRequestRefererUrl(null, "http://localhost:8080"));
        }

    }

}
