package demo.konyushkov.model;

import demo.konyushkov.tools.TestConst;
import demo.konyushkov.tools.ValidatorTest;
import org.junit.jupiter.api.*;

import java.util.UUID;
class AbstractUserValidationTest {

    private AbstractUser user;
    private static ValidatorTest<AbstractUser> validator;

    @BeforeAll
    public static void setUp() {
        validator = new ValidatorTest<>();
    }

    private AbstractUser createValidUser() {
        AbstractUser user = new AbstractUser();
        user.setUuid(UUID.randomUUID());
        user.setEmail(TestConst.EMAIL);
        user.setPassword(TestConst.PASSWORD_HASH);

        return user;
    }

    @Nested
    public class DefaultGroup {


        @BeforeEach
        public void init() {
            user = createValidUser();
        }

        @Nested
        public class Success {
            @Test
            public void When_AbstractUserValid_Expect_ViolationsIsEmpty() {
                validator.successValidation(user);
            }
        }

        @Nested
        public class Failed {
            @AfterEach
            public void test() {
                validator.failedValidation(user);
            }

            @Test
            public void When_EmailIsNull_Expect_OneViolationWithNotNullMessageTemplate() {
                user.setEmail(null);
                validator.fillTestData("email", "{NotNull.AbstractUser.email}");
            }

            @Test
            public void When_EmailIsInvalid_Expect_OneViolationWithEmailMessageTemplate() {
                user.setEmail("invalid_email");
                validator.fillTestData("email", "{demo.konyushkov.validation.constraints.Email}");
            }

            @Test
            public void When_PasswordIsNull_Expect_OneViolationWithNotNullMessageTemplate() {
                user.setPassword(null);
                validator.fillTestData("password", "{NotNull.AbstractUser.password}");
            }

            @Test
            public void When_PasswordIsInvalid_Expect_OneViolationWithPasswordMessageTemplate() {
                user.setPassword("invalid_password");
                validator.fillTestData("password", "{demo.konyushkov.validation.constraints.Password}");
            }
        }
    }
}
