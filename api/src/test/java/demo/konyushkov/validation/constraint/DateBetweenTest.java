package demo.konyushkov.validation.constraint;

import demo.konyushkov.validation.constraints.DateBetween;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.LocalDate;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DateBetweenTest {


    private static Validator validator;
    private Entity entity;
    String messageTemplate = "{demo.konyushkov.validation.constraints.DateBetween}";


    @BeforeAll
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Nested
    public class Success {

        @AfterEach
        public void test() {
            Set<ConstraintViolation<Entity>> violations = validator.validate(entity);
            assertTrue(violations.isEmpty());
        }

        @Test
        void When_DateIsNull_Expect_ViolationsIsEmpty() {
            createEntity(null);
        }

        @Test
        void When_DateBetweenMinAndMax_Expect_ViolationsIsEmpty() {
            createEntity("1999-12-31");
        }

        @Test
        void When_DateEqualsMin_Expect_ViolationsIsEmpty() {
            createEntity("1999-01-01");
        }

        @Test
        void When_DateEqualsMax_Expect_ViolationsIsEmpty() {
            createEntity("2000-01-01");
        }
    }

    @Nested
    public class Failed {
        @AfterEach
        public void test() {
            Set<ConstraintViolation<Entity>> violations = validator.validate(entity);
            assertEquals(1, violations.size());
            assertEquals(messageTemplate, violations.iterator().next().getMessageTemplate());
            System.out.println(violations.iterator().next());
        }

        @Test
        void When_Date_MoreThanMax_Expect_OneViolationConstraint() {
            createEntity("2001-01-01");
        }

        @Test
        void When_Date_LessThanMin_Expect_OneViolationConstraint() {
            createEntity("1998-01-01");
        }
    }

    private void createEntity(String date) {
        if(date != null) {
            entity = new Entity(LocalDate.parse(date));
        }
        else
            entity = new Entity(null);

    }

    private static class Entity {
        public Entity(LocalDate birthday) {
            this.birthday = birthday;
        }

        @Getter @Setter
        @DateBetween(min = "1999-01-01",
                max = "2000-01-01")
        LocalDate birthday;
    }

}
