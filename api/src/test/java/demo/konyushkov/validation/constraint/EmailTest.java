package demo.konyushkov.validation.constraint;

import demo.konyushkov.tools.TestConst;
import demo.konyushkov.validation.constraints.Email;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EmailTest {


    private static Validator validator;
    private Entity entity;
    String messageTemplate = "{demo.konyushkov.validation.constraints.Email}";


    @BeforeAll
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Nested
    public class Success {
        @AfterEach
        public void test() {
            Set<ConstraintViolation<Entity>> violations = validator.validate(entity);
            assertTrue(violations.isEmpty());
        }

        @Test
        public void When_IsValid_Expect_ViolationsIsEmpty() {
            createEntity(TestConst.EMAIL);
        }

        @Test
        public void When_IsNull_Expect_ViolationsIsEmpty() {
            createEntity(null);
        }
    }

    @Nested
    public class Failed {
        @AfterEach
        public void test() {
            Set<ConstraintViolation<Entity>> violations = validator.validate(entity);
            assertEquals(1, violations.size());
            assertEquals(messageTemplate, violations.iterator().next().getMessageTemplate());
        }

        @Test
        public void When_IsEmpty_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("");
        }

        @Test
        public void When_OnlyWhitespaces_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity(" ");
        }

        @Test
        public void When_StartsWithASpace_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity(" kirill@mail.ru");
        }

        @Test
        public void When_StartsWithATab_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("\tkirill@mail.ru");
        }

        @Test
        public void When_EndWithASpace_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("kirill@mail.ru ");
        }

        @Test
        public void When_HasSpaceFirstOption_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("kirill @mail.ru");
        }

        @Test
        public void When_HasSpaceSecondOption_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("kirill@ mail.ru");
        }

        @Test
        public void  When_HasSpaceThirdOption_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("kirill@mail .ru");
        }

        @Test
        public void When_HasSpaceFourthOption_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("kirill@mail. ru");
        }

        @Test
        public void When_MissingLogin_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("@mail.ru");
        }

        @Test
        public void When_MissingDomain_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("@mail.ru");
        }

        @Test
        public void When_MissingAt_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("kirill.ru");
        }

        @Test
        public void When_MissingDot_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("kirill@mailru");
        }
    }

    private void createEntity(String email) {
        entity = new Entity(email);
    }

    private static class Entity {
        public Entity(String email) {
            this.email = email;
        }

        @Getter @Setter @Email String email;
    }

}
