package demo.konyushkov.validation.constraint;

import demo.konyushkov.tools.TestConst;
import demo.konyushkov.validation.constraints.Password;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PasswordTest {


    private static Validator validator;
    private Entity entity;
    String messageTemplate = "{demo.konyushkov.validation.constraints.Password}";


    @BeforeAll
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Nested
    public class Success {

        @AfterEach
        public void test() {
            Set<ConstraintViolation<Entity>> violations = validator.validate(entity);
            assertTrue(violations.isEmpty());
        }

        @Test
        public void When_PasswordIsValid_Expect_ViolationsIsEmpty() {
            createEntity(TestConst.PASSWORD_HASH);
        }

        @Test
        public void When_IsNull_Expect_ViolationsIsEmpty() {
            createEntity(null);
        }
    }

    @Nested
    public class Failed {
        @AfterEach
        public void test() {
            Set<ConstraintViolation<Entity>> violations = validator.validate(entity);
            assertEquals(1, violations.size());
            assertEquals(messageTemplate, violations.iterator().next().getMessageTemplate());
        }

        @Test
        public void When_ShorterThen60Symbols_Expect_OneViolationConstraint() {
            createEntity(TestConst.PASSWORD_HASH.substring(0, 59));
        }

        @Test
        public void When_StartNotWith$2_Expect_OneViolationConstraint() {
            createEntity(TestConst.PASSWORD_HASH.replace("$2", "$1"));
        }

        @Test
        public void When_StartWithSpaces_Expect_OneViolationConstraint() {
            createEntity(" " + TestConst.PASSWORD_HASH.substring(0, 59));
        }

        @Test
        public void When_StartWithTab_Expect_OneViolationConstraint() {
            createEntity("\t" + TestConst.PASSWORD_HASH.substring(0, 59));
        }

        @Test
        public void When_EndWithSpace_Expect_OneViolationConstraint() {
            createEntity(TestConst.PASSWORD_HASH.substring(0, 59) + " ");
        }

        @Test
        public void When_EndWithTab_Expect_OneViolationConstraint() {
            createEntity(TestConst.PASSWORD_HASH.substring(0, 59) + " \t");
        }

        @Test
        public void When_ContainsSpace_Expect_OneViolationConstraint() {
            createEntity(TestConst.PASSWORD_HASH.substring(0, 5) + " " + TestConst.PASSWORD_HASH.substring(7, 60));
        }

        @Test
        public void When_ContainsTab_Expect_OneViolationConstraint() {
            createEntity(TestConst.PASSWORD_HASH.substring(0, 5) + "\t" + TestConst.PASSWORD_HASH.substring(7, 60));
        }
    }

    private void createEntity(String password) {
        entity = new Entity(password);
    }

    private static class Entity {
        public Entity(String password) {
            this.password = password;
        }

        @Getter @Setter @Password String password;
    }

}
