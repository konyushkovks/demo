package demo.konyushkov.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class StringTrimDeserializerTest {

    private ObjectMapper mapper;
    private StringTrimDeserializer deserializer;

    @BeforeEach
    public void setup() {
        mapper = new ObjectMapper();
        deserializer = new StringTrimDeserializer();
    }

    @Test
    public void When_StringContainsTrailingSpace_Expect_TrimString() {
        String testStr = "String with railing space     ";
        String value = deserialize(testStr);
        assertEquals(testStr.trim(), value);
    }

    @Test
    public void When_StringContainsZeroSpace_Expect_TrimString() {
        String testStr = "       String with railing space";
        String value = deserialize(testStr);
        assertEquals(testStr.trim(), value);
    }

    @Test
    public void When_StringContainsDoubleSpace_Expect_TrimStringWithDoubleSpace() {
        String testStr = "String  with  double  space";
        String value = deserialize(testStr);
        assertEquals(testStr.trim(), value);
    }

    @Test
    public void When_StringEmpty_Expect_ReturnNull() {
        String testStr = "";
        String value = deserialize(testStr);
        assertNull(value);
    }

    @Test
    public void When_Null_Expect_ReturnNull() {
        String value = deserialize(null);
        assertNull(value);
    }


    private String obtainJson(String str) {
        if(str != null) {
            return String.format("{\"value\":\"%s\"}", str);
        }

        return "{\"value\": null}";
    }

    private String deserialize(String str) {
        try {
            String json = obtainJson(str);
            InputStream stream = new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8));
            JsonParser parser = mapper.getFactory().createParser(stream);
            DeserializationContext ctx = mapper.getDeserializationContext();
            parser.nextToken();
            parser.nextToken();
            parser.nextToken();
            return deserializer.deserialize(parser, ctx);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
