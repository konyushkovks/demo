package demo.konyushkov.service;

import demo.konyushkov.tools.TestConst;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class AttemptServiceTest {

    private AttemptService attemptService;
    private int MAX_ATTEMPT = 10;
    private int duration = 10;
    private TimeUnit timeUnit = TimeUnit.HOURS;

    @BeforeEach
    void init() {
        this.attemptService = new AttemptServiceImpl(MAX_ATTEMPT, duration, timeUnit);
    }

    @Nested
    public class Failed {
        @Test
        void When_FailedAttemptsIsEmpty_Expect_AttemptsReturnOneFailedAttempt() {
            assertEquals(0, attemptService.getAttempts(TestConst.IP), "Cash not contain attempts");

            attemptService.failed(TestConst.IP);
            assertEquals(1, attemptService.getAttempts(TestConst.IP), "Cash contain 1 attempt");
        }

        @Test
        void When_FailedAttemptsIsNotEmpty_Expect_IncrementFailedAttempts() {
            attemptService.failed(TestConst.IP);
            attemptService.failed(TestConst.IP);
            assertEquals(2, attemptService.getAttempts(TestConst.IP), "Cash contain 2 attempt");
        }
    }

    @Nested
    public class Success {
        @Test
        public void When_FailedAttemptsIsEmpty_Expect_AttemptsCashInvalidate() {
            attemptService.succeeded(TestConst.IP);
            assertEquals(0, attemptService.getAttempts(TestConst.IP), "Success and cash invalidate");
        }

        @Test
        public void When_FailedAttemptsIsNotEmpty_Expect_AttemptsCashInvalidate() {
            attemptService.failed(TestConst.IP);
            attemptService.succeeded(TestConst.IP);
            assertEquals(0, attemptService.getAttempts(TestConst.IP), "Success and cash invalidate");
        }
    }

    @Nested
    public class IsBlocked {
        @Test
        void When_FailedAttemptsIsEmpty_Expect_ReturnFalse() {
            assertFalse(attemptService.isBlocked(TestConst.IP));
        }

        @Test
        void When_FailedAttemptsLessThenMaxAttempts_Expect_ReturnFalse() {
            IntStream.range(0, MAX_ATTEMPT-1).forEach((i) -> attemptService.failed(TestConst.IP));
            assertFalse(attemptService.isBlocked(TestConst.IP));
        }

        @Test
        void When_FailedAttemptsEqualsMaxAttempts_Expect_ReturnTrue() {
            IntStream.range(0, MAX_ATTEMPT).forEach((i) -> attemptService.failed(TestConst.IP));
            assertTrue(attemptService.isBlocked(TestConst.IP));
        }

        @Test
        void When_FailedAttemptsMoreThenMaxAttempts_Expect_ReturnTrue() {
            IntStream.range(0, MAX_ATTEMPT+1).forEach((i) -> attemptService.failed(TestConst.IP));
            assertTrue(attemptService.isBlocked(TestConst.IP));
        }
    }
}
