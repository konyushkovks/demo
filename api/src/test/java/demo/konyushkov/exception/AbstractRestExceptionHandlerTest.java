package demo.konyushkov.exception;

import demo.konyushkov.service.MessageService;
import demo.konyushkov.tools.ValidatorTest;
import demo.konyushkov.validation.constraints.Email;
import demo.konyushkov.validation.constraints.Password;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class AbstractRestExceptionHandlerTest {

    @Mock
    MessageService messageService;
    AbstractRestExceptionHandler exceptionHandler;
    private static ResponseEntity responseEntity;

    private static final String CVE_TITLE = "AbstractRestExceptionHandler.ConstraintViolationException.title";
    private static final String CVE_MESSAGE = "AbstractRestExceptionHandler.ConstraintViolationException.message";

    private static final String NOT_FOUND_TITLE = "AbstractRestExceptionHandler.NotFoundException.title";
    private static final String NOT_FOUND_MESSAGE = "AbstractRestExceptionHandler.NotFoundException.message";

    private static final String ALL_EXCEPTION_TITLE = "AbstractRestExceptionHandler.Exception.title";
    private static final String ALL_EXCEPTION_MESSAGE = "AbstractRestExceptionHandler.Exception.message";

    @BeforeEach
    void init() {
        exceptionHandler = new AbstractRestExceptionHandler(messageService){};
        Mockito.when(messageService.get(CVE_TITLE)).thenReturn(CVE_TITLE);
        Mockito.when(messageService.get(CVE_MESSAGE)).thenReturn(CVE_MESSAGE);

        Mockito.when(messageService.get(NOT_FOUND_TITLE)).thenReturn(NOT_FOUND_TITLE);
        Mockito.when(messageService.get(NOT_FOUND_MESSAGE)).thenReturn(NOT_FOUND_MESSAGE);

        Mockito.when(messageService.get(ALL_EXCEPTION_TITLE)).thenReturn(ALL_EXCEPTION_TITLE);
        Mockito.when(messageService.get(ALL_EXCEPTION_MESSAGE)).thenReturn(ALL_EXCEPTION_MESSAGE);
    }

    @Nested
    public class HandleMethodArgumentNotValid {
        @Test
        void When_Handle_Expect_ReturnResponseWithBadRequestStatusAndNoticeWithConstraintViolationExceptionInfoAndMapWithInvalidFieldsNameAndDescription() {
            MethodArgumentNotValidException ex = PrepareTools.prepareMethodArgumentNotValidException();
            responseEntity = exceptionHandler.handleMethodArgumentNotValid(ex);

            AssertionTools.assertResponseEntity(HttpStatus.BAD_REQUEST, CVE_TITLE, CVE_MESSAGE,
                    PrepareTools.prepareErrors());
        }
    }

    @Nested
    public class HandleConstraintViolationException {

        @Test
        void When_Handle_Expect_ReturnResponseWithBadRequestStatusAndNoticeWithConstraintViolationExceptionInfoAndMapWithInvalidFieldsNameAndDescription() {
            ConstraintViolationException ex = PrepareTools.prepareConstraintViolationException();
            responseEntity = exceptionHandler.handleConstraintValidationException(ex);

            AssertionTools.assertResponseEntity(HttpStatus.BAD_REQUEST, CVE_TITLE, CVE_MESSAGE,
                    PrepareTools.prepareErrors());
        }
    }

    @Nested
    public class HandleNotFoundException {

        @Test
        void When_Handle_Expect_ReturnResponseEntityWithNotFoundStatusAndNoticeWithNotFoundExceptionInfo() {
            responseEntity = exceptionHandler.handleNotFoundException(new NotFoundException());
            AssertionTools.assertResponseEntity(HttpStatus.NOT_FOUND,
                    NOT_FOUND_TITLE, NOT_FOUND_MESSAGE,
                    null);
        }
    }

    @Nested
    public class HandleAllException {

        @Test
        void When_Handle_Expect_ReturnResponseEntityWithInternalServerErrorStatusAndNoticeWithServerErrorInfo() {
            responseEntity = exceptionHandler.handleAllException(new Exception());
            AssertionTools.assertResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR,
                    ALL_EXCEPTION_TITLE,
                    ALL_EXCEPTION_MESSAGE,
                    null);
        }
    }

    @Nested
    public class HandleTransactionSystemException {

        @Test
        void When_CauseIsConstraintViolationException_Expect_HandleViaConstraintViolationExceptionHandler() {
            TransactionSystemException ex = new TransactionSystemException("", PrepareTools.prepareConstraintViolationException());
            responseEntity = exceptionHandler.handleTransactionSystemException(ex);

            AssertionTools.assertResponseEntity(HttpStatus.BAD_REQUEST, CVE_TITLE, CVE_MESSAGE,
                    PrepareTools.prepareErrors());
        }

        @Test
        void When_CauseIsMethodArgumentNotValid_Expect_HandleViaMethodArgumentNotValidHandler() {
            TransactionSystemException ex = new TransactionSystemException("", PrepareTools.prepareMethodArgumentNotValidException());
            responseEntity = exceptionHandler.handleTransactionSystemException(ex);

            AssertionTools.assertResponseEntity(HttpStatus.BAD_REQUEST, CVE_TITLE, CVE_MESSAGE,
                    PrepareTools.prepareErrors());
        }

        @Test
        void When_CauseIsNotFoundException_Expect_HandleViaNotFoundExceptionHandler() {
            TransactionSystemException ex = new TransactionSystemException("", new NotFoundException());
            responseEntity = exceptionHandler.handleTransactionSystemException(ex);
            AssertionTools.assertResponseEntity(HttpStatus.NOT_FOUND,
                    NOT_FOUND_TITLE, NOT_FOUND_MESSAGE,
                    null);
        }


        @Test
        void When_CauseIsOtherException_Expect_HandleViaAllExceptionHandler() {
            TransactionSystemException ex = new TransactionSystemException("", new Exception());
            responseEntity = exceptionHandler.handleAllException(ex);

            AssertionTools.assertResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR,
                    ALL_EXCEPTION_TITLE,
                    ALL_EXCEPTION_MESSAGE,
                    null);
        }
    }

    private static class AssertionTools {

        static void assertResponseEntity(HttpStatus expectedHttpStatus,
                                         String expectedNoticeTitle, String expectedNoticeMessage,
                                         Map<String, Set<String>> expectedErrors) {
            ErrorDetails errorDetails = (ErrorDetails) responseEntity.getBody();
            assertEquals(expectedHttpStatus, responseEntity.getStatusCode());
            assertEquals(expectedNoticeTitle, errorDetails.getNotice().getTitle());
            assertEquals(expectedNoticeMessage, errorDetails.getNotice().getMessage());
            assertEquals(expectedErrors, errorDetails.getErrors());
        }
    }

    private static class PrepareTools {
        private static Validator validator = ValidatorTest.getValidator();

        static Entity createEntity() {
            Entity entity = new Entity();
            entity.setEmail("not_valid_email");
            entity.setPassword("not_valid_password");

            return entity;
        }

        static ConstraintViolationException prepareConstraintViolationException() {
            Entity entity = createEntity();
            Set<ConstraintViolation<Entity>> violations = validator.validate(entity);
            return new ConstraintViolationException(violations);
        }

        static MethodArgumentNotValidException prepareMethodArgumentNotValidException() {
            Entity entity = createEntity();

            SpringValidatorAdapter adapter = new SpringValidatorAdapter(validator);
            BindingResult bindingResult= new BeanPropertyBindingResult(entity, "entity");
            adapter.validate(entity, bindingResult);

            return new MethodArgumentNotValidException(null, bindingResult);
        }

        static Map<String, Set<String>> prepareErrors() {
            Map<String, Set<String>> errors = new HashMap<>();
            errors.put("email", new HashSet<>(Collections.singletonList("{demo.konyushkov.validation.constraints.Email}")));
            errors.put("password", new HashSet<>(Collections.singletonList("{demo.konyushkov.validation.constraints.Password}")));

            return errors;
        }
    }


    @Getter @Setter
    private static class Entity {

        @Email private String email;

        @Password private String password;

    }
}
