package demo.konyushkov.captcha;

import demo.konyushkov.captcha.exception.ReCaptchaInvalidException;
import demo.konyushkov.captcha.exception.ReCaptchaUnavailableException;
import demo.konyushkov.helper.HttpHelper;
import demo.konyushkov.service.AttemptService;
import demo.konyushkov.service.MessageService;
import demo.konyushkov.tools.TestConst;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class CaptchaServiceImplTest {

    private CaptchaService captchaService;
    @Mock private GoogleResponse googleResponse;
    @Mock private CaptchaSettings captchaSettings;
    @Mock private AttemptService captchaAttemptService;
    @Mock private RestOperations restTemplate;
    @Mock private MessageService messageService;
    @Mock private HttpServletRequest request;

    private final String secret = "validSecret";
    private final String response = "validResponse";
    private final String notValidResponse = "#not valid";
    private int failedAttempts = 0;

    @BeforeEach
    void init() {
        captchaService = new CaptchaServiceImpl(
                captchaSettings, captchaAttemptService,
                messageService, restTemplate, () -> true);
        failedAttempts = 0;

        Mockito.when(request.getRemoteAddr()).thenReturn(TestConst.IP);
        Mockito.when(captchaSettings.getSite()).thenReturn(TestConst.SOCKET);
        Mockito.when(captchaSettings.getSecret()).thenReturn(secret);
    }


    @Nested
    public class GetReCaptchaSite {
        @Test
        void When_CaptchaSettingContentReCaptchaSite_Expect_ReturnReCaptchaSite() {
            assertEquals(captchaSettings.getSite(), captchaService.getReCaptchaSite());
        }
    }

    @Nested
    public class GetReCaptchaSecret {
        @Test
        void When_CaptchaSettingContentReCaptchaSecret_Expect_ReturnReCaptchaSecret() {
            assertEquals(captchaSettings.getSecret(), captchaService.getReCaptchaSecret());
        }
    }

    @Nested
    public class ProcessResponse {

        private final String BLOCKED = "CaptchaService.ReCaptchaInvalidException.blocked";
        private final String NOT_SANITY = "CaptchaService.ReCaptchaInvalidException.notSanity";
        private final String NOT_VALID = "CaptchaService.ReCaptchaInvalidException.notValid";
        private final String RCE = "CaptchaService.ReCaptchaUnavailableException.rce";


        @BeforeEach
        public void init() {
            Mockito.when(captchaAttemptService.isBlocked(TestConst.IP)).thenReturn(false);
            Mockito.doAnswer((i) -> failedAttempts++).when(captchaAttemptService).failed(TestConst.IP);
            Mockito.doAnswer((i) -> failedAttempts = 0).when(captchaAttemptService).succeeded(TestConst.IP);

            Mockito.when(googleResponse.isSuccess()).thenReturn(true);
            Mockito.when(googleResponse.hasClientError()).thenReturn(false);
            Mockito.when(googleResponse.toString()).thenReturn("Google response to string");

            Mockito.when(messageService.get(eq(BLOCKED), anyString())).thenReturn(BLOCKED);
            Mockito.when(messageService.get(eq(NOT_SANITY), anyString())).thenReturn(NOT_SANITY);
            Mockito.when(messageService.get(eq(NOT_VALID), anyString())).thenReturn(NOT_VALID);
            Mockito.when(messageService.get(eq(RCE), anyString())).thenReturn(RCE);

            Mockito.when(HttpHelper.getReCaptchaResponse(request)).thenReturn(response);
        }


        @Test
        void When_ClientIsBlocked_Expect_ThrowReCaptchaInvalidExceptionAndFailedAttemptsNotIncrement() {
            Mockito.when(captchaAttemptService.isBlocked(TestConst.IP)).thenReturn(true);

            Throwable throwable = assertThrows(ReCaptchaInvalidException.class,
                    () -> captchaService.processResponse(request));

            assertEquals(BLOCKED, throwable.getMessage());
            assertEquals(0, failedAttempts);
        }

        @Test
        void When_ResponseIsNotSanity_ThrowReCaptchaInvalidExceptionAndIncrementFailedAttempts() {
            Mockito.when(HttpHelper.getReCaptchaResponse(request)).thenReturn(notValidResponse);

            Throwable throwable = assertThrows(ReCaptchaInvalidException.class,
                    () -> captchaService.processResponse(request));

            assertEquals(NOT_SANITY, throwable.getMessage());
            assertEquals(1, failedAttempts);
        }

        @Test
        void When_ResponseIsEmpty_Expect_ThrowReCaptchaInvalidExceptionAndIncrementFailedAttempts() {
            Mockito.when(HttpHelper.getReCaptchaResponse(request)).thenReturn("");

            Throwable throwable = assertThrows(ReCaptchaInvalidException.class,
                    () -> captchaService.processResponse(request));

            assertEquals(NOT_SANITY, throwable.getMessage());
            assertEquals(1, failedAttempts);
        }

        @Test
        void When_ResponseIsNull_Expect_ThrowReCaptchaInvalidExceptionAndIncrementFailedAttempts() {
            Mockito.when(HttpHelper.getReCaptchaResponse(request)).thenReturn(null);

            Throwable throwable = assertThrows(ReCaptchaInvalidException.class,
                    () -> captchaService.processResponse(request));

            assertEquals(NOT_SANITY, throwable.getMessage());
            assertEquals(1, failedAttempts);
        }

        @Test
        void When_ResponseIsValid_Expect_ClearFailedAttempts() {
            failedAttempts = 2;
            Mockito.when(restTemplate.getForObject(any(URI.class), eq(GoogleResponse.class))).thenReturn(googleResponse);
            captchaService.processResponse(request);
            assertEquals(0, failedAttempts);
        }

        @Test
        void When_GoogleResponseIsFailedWithoutClientError_Expect_ThrowReCaptchaInvalidExceptionAndFailedAttemptsNotIncrement() {
            Mockito.when(googleResponse.isSuccess()).thenReturn(false);
            Mockito.when(restTemplate.getForObject(any(URI.class), eq(GoogleResponse.class))).thenReturn(googleResponse);

            Throwable throwable = assertThrows(ReCaptchaInvalidException.class,
                    () -> captchaService.processResponse(request));

            assertEquals(NOT_VALID, throwable.getMessage());
            assertEquals(0, failedAttempts);
        }

        @Test
        void When_GoogleResponseIsFailedWithClientError_Expect_ThrowReCaptchaInvalidExceptionAndIncrementFailedAttempts() {
            Mockito.when(googleResponse.isSuccess()).thenReturn(false);
            Mockito.when(googleResponse.hasClientError()).thenReturn(true);
            Mockito.when(restTemplate.getForObject(any(URI.class), eq(GoogleResponse.class))).thenReturn(googleResponse);

            Throwable throwable = assertThrows(ReCaptchaInvalidException.class,
                    () -> captchaService.processResponse(request));

            assertEquals(NOT_VALID, throwable.getMessage());
            assertEquals(1, failedAttempts);
        }

        @Test
        void When_RestClientThrowRestClientException_Expect_ThrowReCaptchaUnavailableExceptionAndFailedAttemptsNotIncrement() {
            Mockito.when(restTemplate.getForObject(any(URI.class), eq(GoogleResponse.class))).thenThrow(RestClientException.class);

            Throwable throwable = assertThrows(ReCaptchaUnavailableException.class,
                    () -> captchaService.processResponse(request));

            assertEquals(RCE, throwable.getMessage());
            assertEquals(0, failedAttempts);
        }
    }
}
