INSERT INTO authority (name) VALUES ('ROLE_ADMIN'), ('ROLE_SUPERUSER'), ('ROLE_MODERATOR');
INSERT INTO "USER" (uuid, email, name, password, photo, birthday, surname, mobile_phone, note, gender) VALUES ('c56240bc-b006-4197-90e7-4d31a9e601f1', 'test@mail.ru', 'Кирилл', '$2a$04$PXtEDiwr3KfHRzDT66ugBeoSdF6uQZmYH5QGVwirS2yrBMgi3Ngqy', null, '1996-01-27', 'Конюшков', '8 (985) 412-44-37', null, 1);
INSERT INTO users_authorities (authority_name, user_uuid)
  VALUES ('ROLE_ADMIN', 'c56240bc-b006-4197-90e7-4d31a9e601f1'),
        ('ROLE_SUPERUSER', 'c56240bc-b006-4197-90e7-4d31a9e601f1'),
        ('ROLE_MODERATOR', 'c56240bc-b006-4197-90e7-4d31a9e601f1');
