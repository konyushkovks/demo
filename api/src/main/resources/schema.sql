CREATE TABLE "USER"
(
    uuid         UUID NOT NULL
        CONSTRAINT user_pkey
        PRIMARY KEY,
    email        VARCHAR(255)
        CONSTRAINT user_email_ukey
        UNIQUE,
    name         VARCHAR(255),
    password     VARCHAR(255),
    photo        VARCHAR(255),
    birthday     DATE,
    surname      VARCHAR(255),
    mobile_phone VARCHAR(255),
    note         TEXT,
    gender       SMALLINT
);

CREATE TABLE authority
(
    name VARCHAR(255) NOT NULL
        CONSTRAINT authority_pkey
        PRIMARY KEY
);

CREATE TABLE users_authorities
(
    authority_name VARCHAR(255) NOT NULL
        CONSTRAINT users_authorities_authority_name_fkey
        REFERENCES authority,
    user_uuid      UUID         NOT NULL
        CONSTRAINT users_authorities_user_uuid_fkey
        REFERENCES "USER"
);
