package demo.konyushkov.exception;

import demo.konyushkov.dto.Notice;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Map;
import java.util.Set;

@Getter @Setter
class ErrorDetails {

    private HttpStatus status;
    private Notice notice;
    private Map<String, Set<String>> errors;

    ErrorDetails(HttpStatus status, Notice notice) {
        this.status = status;
        this.notice = notice;
    }

    ErrorDetails(HttpStatus status, Notice notice, Map<String, Set<String>> errors) {
        this.status = status;
        this.notice = notice;
        this.errors = errors;
    }
}
