package demo.konyushkov.exception;


import demo.konyushkov.dto.Notice;
import demo.konyushkov.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.*;

@Slf4j
public abstract class AbstractRestExceptionHandler {
    private final MessageService messageService;

    public AbstractRestExceptionHandler(MessageService messageService) {
        this.messageService = messageService;
    }

    /* 400 */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex) {

        final Map<String, Set<String>> errors = ex.getBindingResult().getFieldErrors().stream()
                .collect(groupingBy(
                            FieldError::getField,
                            mapping(FieldError::getDefaultMessage, toSet())));

        log.warn("{}", ex);

        return getConstraintViolationResponse(errors);
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintValidationException(final ConstraintViolationException ex) {

        final Map<String, Set<String>> errors = ex.getConstraintViolations().stream()
                .collect(groupingBy(this::pathToString,
                        mapping(ConstraintViolation::getMessage, toSet())));

        log.warn("{}", ex);

        return getConstraintViolationResponse(errors);
    }

    private ResponseEntity<Object> getConstraintViolationResponse(Map<String, Set<String>> errors) {
        final Notice notice = new Notice(messageService.get("AbstractRestExceptionHandler.ConstraintViolationException.title"),
                messageService.get("AbstractRestExceptionHandler.ConstraintViolationException.message"));
        final ErrorDetails apiError = new ErrorDetails(HttpStatus.BAD_REQUEST, notice, errors);

        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    private String pathToString(ConstraintViolation violation) {
        return violation.getPropertyPath().toString();
    }

    /* 404 */
    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<Object> handleNotFoundException(final NotFoundException ex) {
        log.warn("{}", ex);

        final Notice notice = new Notice(messageService.get("AbstractRestExceptionHandler.NotFoundException.title"),
                messageService.get("AbstractRestExceptionHandler.NotFoundException.message"));
        final ErrorDetails apiError = new ErrorDetails(HttpStatus.NOT_FOUND, notice);

        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    /* 500 */
    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Object> handleAllException(Exception ex) {
        log.error("{}", ex);
        final Notice notice = new Notice("AbstractRestExceptionHandler.Exception.title",
                "AbstractRestExceptionHandler.Exception.message");
        final ErrorDetails apiError = new ErrorDetails(HttpStatus.INTERNAL_SERVER_ERROR, notice);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    /* TransactionSystemException */
    @ExceptionHandler({ TransactionSystemException.class })
    public ResponseEntity<Object> handleTransactionSystemException(final TransactionSystemException ex) {
        log.warn("{}", ex.getClass().getName());

        Throwable cause = ex.getRootCause();

        if (cause instanceof ConstraintViolationException) {
            return handleConstraintValidationException((ConstraintViolationException) cause);
        }
        else if (cause instanceof MethodArgumentNotValidException) {
            return handleMethodArgumentNotValid((MethodArgumentNotValidException) cause);
        }
        else if(cause instanceof NotFoundException) {
            return handleNotFoundException((NotFoundException) cause);
        }
        else {
            return handleAllException(ex);
        }
    }
}
