package demo.konyushkov.service;

public interface MessageService {

    String get(String code);
    String get(String code, String defaultMessage);
}
