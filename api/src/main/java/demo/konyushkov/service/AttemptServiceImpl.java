package demo.konyushkov.service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Slf4j
public class AttemptServiceImpl implements AttemptService {
    private LoadingCache<String, Integer> attemptsCache;
    private int MAX_ATTEMPT;

    public AttemptServiceImpl(int MAX_ATTEMPT, int duration, TimeUnit timeUnit) {
        Assert.isTrue(MAX_ATTEMPT > 1, "MAX_ATTEMPT must be large 1");
        Assert.isTrue(duration > 0, "duration must be positive");
        Assert.notNull(timeUnit, "timeUnit must not be null");

        this.MAX_ATTEMPT = MAX_ATTEMPT;
        attemptsCache = CacheBuilder.newBuilder()
                .expireAfterWrite(duration, timeUnit)
                .build(new CacheLoader<String, Integer>() {
                    @Override
                    public Integer load(final String key) {
                        return 0;
                    }
                });
    }

    public void succeeded(final String ip) {
        log.info(String.format("Success attempt from ip: %s", ip));
        attemptsCache.invalidate(ip);
    }

    public void failed(final String ip) {
        int failedAttempts = getAttempts(ip) + 1;
        log.info(String.format("Failed attempt from ( IP: %s; failed attempts: %s", ip, failedAttempts));
        attemptsCache.put(ip, failedAttempts);
    }

    public boolean isBlocked(String ip) {
        boolean isBlocked = getAttempts(ip) >= MAX_ATTEMPT;

        if(isBlocked) {
            log.info(String.format("Blocked ip: %s", ip));
        }

        return isBlocked;
    }

    public int getAttempts(String key) {
        try {
            return attemptsCache.get(key);
        } catch (ExecutionException e) {
            return 0;
        }
    }
}
