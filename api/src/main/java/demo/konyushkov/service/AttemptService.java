package demo.konyushkov.service;

public interface AttemptService {

    void succeeded(final String ip);

    void failed(final String ip);

    boolean isBlocked(String ip);

    int getAttempts(String key);

}
