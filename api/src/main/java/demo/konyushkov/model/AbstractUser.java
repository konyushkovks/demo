package demo.konyushkov.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import demo.konyushkov.validation.constraints.Email;
import demo.konyushkov.validation.constraints.Password;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;



@Entity
@Getter @Setter
@DiscriminatorFormula("null")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "user",
        uniqueConstraints = @UniqueConstraint(columnNames = "email", name = "user_email_ukey"))
class AbstractUser {

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private UUID uuid;

    /* Контактная информация */
    @Email
    @NotNull(message = "{NotNull.AbstractUser.email}")
    @Column(nullable = false)
    private String email;

    @JsonIgnore
    @Password
    @NotNull(message = "{NotNull.AbstractUser.password}")
    @Column(nullable = false)
    private String password;
}
