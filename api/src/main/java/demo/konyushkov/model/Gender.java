package demo.konyushkov.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Gender {
    UNKNOWN,
    MALE,
    FEMALE;

    @JsonValue
    public int toValue() {
        return ordinal();
    }
}
