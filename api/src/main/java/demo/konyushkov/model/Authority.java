package demo.konyushkov.model;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.util.List;

@Entity
@Immutable
@Getter @Setter
@Table(name = "authority")
class Authority {

    @Id
    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "users_authorities", joinColumns = {
        @JoinColumn(name = "authority_name", foreignKey = @ForeignKey(name = "users_authorities_authority_name_fkey"), nullable = false, updatable = false) },
        inverseJoinColumns = { @JoinColumn(name = "user_uuid", foreignKey = @ForeignKey(name = "users_authorities_user_uuid_fkey"),
            nullable = false, updatable = false) })
    private List<AbstractUser> users;
}
