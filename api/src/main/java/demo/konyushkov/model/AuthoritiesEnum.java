package demo.konyushkov.model;

public enum AuthoritiesEnum {
    ROLE_ADMIN,
    ROLE_SUPERUSER,
    ROLE_MODERATOR
}
