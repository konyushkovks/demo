package demo.konyushkov.captcha;

import demo.konyushkov.captcha.exception.ReCaptchaInvalidException;

import javax.servlet.http.HttpServletRequest;

public interface CaptchaService {
    void processResponse(HttpServletRequest request) throws ReCaptchaInvalidException;
    String getReCaptchaSite();
    String getReCaptchaSecret();
    boolean isCaptchaShow();
}
