package demo.konyushkov.captcha;

import demo.konyushkov.captcha.exception.ReCaptchaInvalidException;
import demo.konyushkov.captcha.exception.ReCaptchaUnavailableException;
import demo.konyushkov.helper.HttpHelper;
import demo.konyushkov.service.AttemptService;
import demo.konyushkov.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.function.Supplier;
import java.util.regex.Pattern;

@Slf4j
public class CaptchaServiceImpl implements CaptchaService {
    private static final Pattern RESPONSE_PATTERN = Pattern.compile("[A-Za-z0-9_-]+");
    private final CaptchaSettings captchaSettings;
    private final AttemptService captchaAttemptService;
    private final RestOperations restTemplate;
    private Supplier<Boolean> isShowSupplier;
    private MessageService messageService;

    public CaptchaServiceImpl(CaptchaSettings captchaSettings,
                              AttemptService captchaAttemptService,
                              MessageService messageService,
                              RestOperations restTemplate,
                              Supplier<Boolean> isShowSupplier) {
        this.captchaSettings = captchaSettings;
        this.messageService = messageService;
        this.captchaAttemptService = captchaAttemptService;
        this.restTemplate = restTemplate;
        this.isShowSupplier = isShowSupplier;
    }

    @Override
    public void processResponse(HttpServletRequest request) {
        String response = HttpHelper.getReCaptchaResponse(request);
        String clientIp = HttpHelper.getClientIP(request);

        log.debug("Attempting to validate response {}", response);

        if (captchaAttemptService.isBlocked(clientIp)) {
            throw new ReCaptchaInvalidException(messageService.get("CaptchaService.ReCaptchaInvalidException.blocked",
                    "Client exceeded maximum number of failed attempts"));
        }

        if (!responseSanityCheck(response)) {
            captchaAttemptService.failed(clientIp);
            throw new ReCaptchaInvalidException(messageService.get("CaptchaService.ReCaptchaInvalidException.notSanity",
                    "reCAPTCHA: Response contains invalid characters"));
        }

        final URI verifyUri = URI.create(String.format("https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s",
                getReCaptchaSecret(), response, clientIp));
        try {
            final GoogleResponse googleResponse = restTemplate.getForObject(verifyUri, GoogleResponse.class);
            log.debug("Google's response: {} ", googleResponse.toString());

            if (!googleResponse.isSuccess()) {
                if (googleResponse.hasClientError()) {
                    captchaAttemptService.failed(clientIp);
                }
                throw new ReCaptchaInvalidException(messageService.get("CaptchaService.ReCaptchaInvalidException.notValid",
                        "reCaptcha was not successfully validated"));
            }

            captchaAttemptService.succeeded(clientIp);
        } catch (RestClientException rce) {
            throw new ReCaptchaUnavailableException(messageService.get("CaptchaService.ReCaptchaUnavailableException.rce",
                    "System error.  Please try again later."));
        }
    }

    private boolean responseSanityCheck(final String response) {
        return StringUtils.hasLength(response) && RESPONSE_PATTERN.matcher(response).matches();
    }

    @Override
    public String getReCaptchaSite() {
        return captchaSettings.getSite();
    }

    @Override
    public String getReCaptchaSecret() {
        return captchaSettings.getSecret();
    }

    @Override
    public boolean isCaptchaShow() {
        if(isShowSupplier == null) {
            return true;
        }
        return isShowSupplier.get();
    }
}
