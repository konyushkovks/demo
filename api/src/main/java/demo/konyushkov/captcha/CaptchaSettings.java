package demo.konyushkov.captcha;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
public class CaptchaSettings {
    private String site;
    private String secret;
}
