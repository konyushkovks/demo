package demo.konyushkov.validation.constraints;

import demo.konyushkov.validation.validator.EmailValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = EmailValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Email {

    String message() default "{demo.konyushkov.validation.constraints.Email}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
