package demo.konyushkov.validation.validator;

import demo.konyushkov.validation.constraints.DateBetween;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.util.Optional;

public class DateBetweenValidator implements
        ConstraintValidator<DateBetween, LocalDate> {

    private DateBetween dateBetween;

    @Override
    public void initialize(DateBetween dateBetween) {
        this.dateBetween = dateBetween;
    }

    @Override
    public boolean isValid(LocalDate value, ConstraintValidatorContext context) {

        return Optional.ofNullable(value).map((v) -> {
            LocalDate min = Optional.of(dateBetween.min()).map(LocalDate::parse).orElse(LocalDate.MIN);
            LocalDate max = Optional.of(dateBetween.max()).map(LocalDate::parse).orElse(LocalDate.MAX);
            return !v.isBefore(min) && !v.isAfter(max);
        }).orElse(true);
    }
}
