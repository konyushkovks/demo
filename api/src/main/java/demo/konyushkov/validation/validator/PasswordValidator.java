package demo.konyushkov.validation.validator;

import demo.konyushkov.validation.constraints.Password;

import java.util.regex.Pattern;

public class PasswordValidator extends CustomPatternConstraintValidator<Password> {

    public PasswordValidator() {
        super(Pattern.compile("^\\$2[ayb]\\$[\\S]{56}$"));
    }
}
