package demo.konyushkov.validation.validator;

import demo.konyushkov.validation.constraints.Email;

import java.util.regex.Pattern;

public class EmailValidator extends CustomPatternConstraintValidator<Email> {

    public EmailValidator() {
        super(Pattern.compile("^[\\S]+@[\\S]+\\.[\\S]+$"));
    }
}
