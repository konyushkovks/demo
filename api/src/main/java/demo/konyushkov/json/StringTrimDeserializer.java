package demo.konyushkov.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

public class StringTrimDeserializer extends StdScalarDeserializer<String> {
    private static final long serialVersionUID = 1L;

    public StringTrimDeserializer() {
        super(String.class);
    }

    @Override
    public String deserialize(JsonParser jsonParser, DeserializationContext ctx) throws IOException {

        return StringUtils.trimToNull(jsonParser.getValueAsString());
    }
}
