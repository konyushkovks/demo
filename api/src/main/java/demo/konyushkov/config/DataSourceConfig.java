package demo.konyushkov.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {

    private final Environment environment;

    @Autowired
    public DataSourceConfig(Environment environment) {
        this.environment = environment;
    }


    @Bean
    public DataSource dataSource() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(environment.getRequiredProperty("jdbc.url"));
        config.setUsername(environment.getRequiredProperty("jdbc.username"));
        config.setPassword(environment.getRequiredProperty("jdbc.password"));
        config.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
        config.setMaximumPoolSize(Integer.parseInt(environment.getRequiredProperty("hibernate.hikari.maximumPoolSize")));
        config.setIdleTimeout(Long.parseLong(environment.getRequiredProperty("hibernate.hikari.idleTimeout")));
        config.setPoolName(environment.getRequiredProperty("hibernate.hikari.poolName"));

        return new HikariDataSource(config);
    }
}
