package demo.konyushkov.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class Notice {

    private String title;
    private String message;

    public Notice(String title, String message) {
        this.title = title;
        this.message = message;
    }
}
