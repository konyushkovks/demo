package demo.konyushkov.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class ApiResponse<T> {

    private Notice notice;
    private T payload;

    public ApiResponse(Notice notice, T payload) {
        this.notice = notice;
        this.payload = payload;
    }
}
