package demo.konyushkov.helper;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import java.util.UUID;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AuthenticationHelper {

    public static Authentication getAuthentication(SecurityContext context) {
        return context.getAuthentication();
    }

    public static boolean isAnonymous(SecurityContext context) {
        return isAnonymous(getAuthentication(context));
    }

    public static boolean isAnonymous(Authentication authentication) {
        try {
            boolean isAnonymous = authentication.getAuthorities()
                    .contains(new SimpleGrantedAuthority("ROLE_ANONYMOUS"));

            if(!isAnonymous) {
                log.info("User {} is not anonymous", authentication.getName());
            }

            return isAnonymous;
        }
        catch (Exception e) {
            log.error("{}: Determination of isAnonymous failed. Cause: {}", e.getClass().getName(), e);
            return true;
        }
    }

    public static UUID getUUID(OAuth2Authentication authentication) {
        return UUID.fromString((String) authentication.getPrincipal());
    }
}
