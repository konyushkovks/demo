package demo.konyushkov.tools;

import lombok.Getter;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.groups.Default;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidatorTest<T> {

    @Getter
    private static Validator validator;
    private String testedField;
    private String messageTemplate;

    static  {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    public void fillTestData(String testedField, String messageTemplate) {
        this.testedField = testedField;
        this.messageTemplate = messageTemplate;
    }

    public void successValidation(T instance) {
        successValidation(instance, Default.class);
    }

    public void successValidation(T instance, Class group) {
        Set<ConstraintViolation<T>> violations = validator.validate(instance, group);
        assertTrue(violations.isEmpty());
    }

    public void failedValidation(T instance) {
        failedValidation(instance, Default.class);
    }

    public void failedValidation(T instance, Class group) {
        Set<ConstraintViolation<T>> violations = validator.validate(instance, group);
        assertEquals(1, violations.size());
        ConstraintViolation violation = violations.iterator().next();
        assertEquals(testedField, violation.getPropertyPath().toString());
        assertEquals(messageTemplate, violation.getMessageTemplate());
    }
}
