package demo.konyushkov.tools;


public class TestConst {

    /* User */
    public static final java.util.UUID UUID = java.util.UUID.fromString("c56240bc-b006-4197-90e7-4d31a9e601f1");
    public static final String EMAIL = "test@mail.ru";
    public static final String PASSWORD_HASH = "$2a$04$PXtEDiwr3KfHRzDT66ugBeoSdF6uQZmYH5QGVwirS2yrBMgi3Ngqy";
    public static final String PASSWORD = "admin";

    public static final String IP = "192.168.0.1";
    public static final String SOCKET = "localhost:8000";

    /* Missing data */
    public static final String MISSING_EMAIL = "missing@email.ru";

    /* Wrong data */
    public static final String WRONG_PASSWORD = "wrong_password";
}
