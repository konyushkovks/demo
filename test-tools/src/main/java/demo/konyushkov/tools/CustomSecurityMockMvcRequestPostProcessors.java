package demo.konyushkov.tools;

import org.springframework.test.web.servlet.request.RequestPostProcessor;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;

public class CustomSecurityMockMvcRequestPostProcessors {
    public static RequestPostProcessor admin() {
        return user(TestConst.EMAIL).password(TestConst.PASSWORD);
    }
}
