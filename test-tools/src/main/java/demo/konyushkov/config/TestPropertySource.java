package demo.konyushkov.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
    @Profile("test")
    @PropertySource(value = {"classpath:application-shared.properties",
                    "classpath:application-shared-test.properties",
                    "classpath:application.properties",
                    "classpath:application-test.properties"}, ignoreResourceNotFound = true)
public class TestPropertySource {}
